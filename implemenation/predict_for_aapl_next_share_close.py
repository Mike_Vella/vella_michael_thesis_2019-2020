import pandas as pd
import sklearn.metrics as metrics
from keras.layers import Dense, LSTM
from keras.models import Sequential
from keras.models import save_model as k_save_model
from sqlalchemy import create_engine

def min_max_normilisation(dataframe):
    dataframe_normalised = (dataframe - dataframe.min()) / (dataframe.max() - dataframe.min())

    dataframe_normalised = dataframe_normalised.dropna()
    return dataframe_normalised


def split_dataset(dataset, test_size, seed):
    train = dataset.sample(frac=test_size, random_state=seed)
    # sampling removes the data present in the dataframe
    # Drops columns that contain the training data, and samples the full remainder of data.
    test = dataset.drop(train.index).sample(frac=1.0)
    return train, test


def reverse_min_max_normilisation(dataframe):
    if 0 in dataframe['predicted_close']:
        dataframe['denormalised_pred_close'] = dataframe['close'] * (1 + dataframe['predicted_close'])
    else:
        dataframe['denormalised_pred_close'] = dataframe['predicted_close'] / dataframe['normalised_closed'] * dataframe['close']
        dataframe = dataframe['denormalised_pred_close'].round(2)
    return dataframe


def get_x_y(dataframe, yColumn):
    y = dataframe[f'{yColumn}']
    x = dataframe.drop(axis=1, labels=f'{yColumn}')

    xnumpy = pd.DataFrame.to_numpy(x)
    ynumpy = pd.DataFrame.to_numpy(y)

    shape_of_x = xnumpy.shape
    xnumpy = xnumpy.reshape((shape_of_x[0], 1, shape_of_x[1]))

    return xnumpy, ynumpy


def rmse(ytest, pred):
    return metrics.mean_squared_error(ytest, pred, squared=False)


def run_model(x_train, y_train, x_test, y_test,
              input_shape: tuple, compile_loss: str
              , epcohs: int):
    rnn = Sequential()
    rnn.add(LSTM(units=60, return_sequences=True, input_shape=input_shape))
    # regressor.add(Dropout(0.1))
    rnn.add(LSTM(units=50, return_sequences=True))
    # regressor.add(Dropout(0.1))
    rnn.add(LSTM(units=40, return_sequences=True))
    # regressor.add(Dropout(0.1))
    rnn.add(LSTM(units=30))
    # regressor.add(Dropout(0.2))
    rnn.add(Dense(units=1))
    rnn.compile(optimizer='adam', loss=compile_loss)
    rnn.fit(x_train, y_train, validation_data=(x_test, y_test),
            epochs=epcohs, batch_size=100)
    model_score = rnn.evaluate(x_test, y_test, batch_size=100)
    aapl_prediction = rnn.predict(aapl_pred_x)
    rmse_value = rmse(y_test,rnn.predict(x_test))
    print(f"\nCurrent Model: appl_pred_model")
    print(f"Overall appl_pred_model loss using MSA: ", model_score)
    print("rmse value :", rmse_value)

    k_save_model(rnn, f'gen_data\\rnn_models\\appl_pred_model.h5')
    return aapl_prediction

   # dictionary_results[model_name] = [rmse_value, 'rnn']


five_yr_stock_sent = pd.read_csv("gen_data/5yr_full_sentiment_aapl_only_export.csv")

five_yr_sent_full_with_senti = five_yr_stock_sent.reset_index(drop=True)
five_yr_sent_full_with_senti = five_yr_sent_full_with_senti.set_index(['tickerName', 'date', 'index'])

five_yr_sent_full_normalised = min_max_normilisation(five_yr_sent_full_with_senti)


aapl_pred_full_with_senti_normalised = five_yr_sent_full_normalised.reset_index()
aapl_pred_full_with_senti_normalised = aapl_pred_full_with_senti_normalised[aapl_pred_full_with_senti_normalised['date'] == '2020-02-14']
aapl_pred_full_with_senti_normalised = aapl_pred_full_with_senti_normalised.set_index(['tickerName', 'date', 'index'])

aapl_pred_x, aapl_pred_y = get_x_y(aapl_pred_full_with_senti_normalised, 'close')

# split dataset
five_yr_sent_full_normalised = five_yr_sent_full_normalised.reset_index()
five_yr_sent_full_normalised = five_yr_sent_full_normalised[five_yr_sent_full_normalised['date'] < '2020-02-14']
five_yr_sent_full_normalised = five_yr_sent_full_normalised.set_index(['tickerName', 'date', 'index'])
train_5yr_senti_70, test_5yr_senti_30 = split_dataset(five_yr_sent_full_normalised, 0.7, 2300)

xtrain_5yr_senti_70, ytrain_5yr_senti_70 = get_x_y(train_5yr_senti_70, 'close')
xtest_5yr_senti_30, ytest_5yr_senti_30 = get_x_y(test_5yr_senti_30, 'close')

predicted = run_model(xtrain_5yr_senti_70, ytrain_5yr_senti_70, xtest_5yr_senti_30, ytest_5yr_senti_30, (1, 15), 'mean_absolute_error', 42)
print(predicted)
predicted_list = list(predicted.flatten(order='K'))
print(predicted_list)

aapl_full_non_normalised = five_yr_stock_sent[five_yr_stock_sent['date'] == '2020-02-14']
aapl_full_non_normalised['predicted_close'] = predicted_list
aapl_full_non_normalised = reverse_min_max_normilisation(aapl_full_non_normalised)
aapl_full_non_normalised.to_csv("gen_data/aapl_pred_results/aapl_predicted.csv",
                index=True, header=True, mode="w", sep=',')
