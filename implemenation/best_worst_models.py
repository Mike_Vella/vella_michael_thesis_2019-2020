import matplotlib.pyplot as plot
import pandas as pd
import sklearn.metrics as metrics
from joblib import load
from keras.models import load_model as keras_load_model


def min_max_normilisation(dataframe):
    dataframe_normalised = (dataframe - dataframe.min()) / (dataframe.max() - dataframe.min())

    dataframe_normalised = dataframe_normalised.dropna()
    return dataframe_normalised


def reverse_min_max_normilisation(dataframe):
    if 0 in dataframe['predicted_close']:
        dataframe['denormalised_pred_close'] = dataframe['close'] * (1 + dataframe['predicted_close'])
    else:
        dataframe['denormalised_pred_close'] = dataframe['predicted_close'] / dataframe['normalised_closed'] * dataframe['close']

    return dataframe


def split_dataset(dataset, test_size, seed):
    train = dataset.sample(frac=test_size, random_state=seed)
    # sampling removes the data present in the dataframe
    # Drops columns that contain the training data, and samples the full remainder of data.
    test = dataset.drop(train.index).sample(frac=1.0)

    return train, test


def get_x_y_keras(dataframe, yColumn):
    y = dataframe[f'{yColumn}']
    x = dataframe.drop(axis=1, labels=f'{yColumn}')

    xnumpy = pd.DataFrame.to_numpy(x)
    ynumpy = pd.DataFrame.to_numpy(y)

    shape_of_x = xnumpy.shape
    xnumpy = xnumpy.reshape((shape_of_x[0], 1, shape_of_x[1]))

    return xnumpy, ynumpy


def get_x_y(dataframe, yColumn):
    y = dataframe[f'{yColumn}']
    x = dataframe.drop(axis=1, labels=f'{yColumn}')

    xnumpy = pd.DataFrame.to_numpy(x)
    ynumpy = pd.DataFrame.to_numpy(y)

    return xnumpy, ynumpy


def get_prediciton_keras(actual_model, xtest):
    '''
    :param dict_model: dictionary of models
    :param ytest: only pass ytest if the previous models used the same ytest
    '''
    # model_score = actual_model.evaluate(xtest, ytest, batch_size=100)
    prediction = actual_model.predict(xtest)
    # test_values = actual_model.test_on_batch(xtest, ytest)
    # rmse_value = rmse(ytest, prediction)
    # print(f"\nCurrent Model: {model_name}")
    # print(f"Overall {model_name} loss using MSA: ", model_score)
    # print(f"RMSE : ", rmse(ytest, prediction))
    # print(f"MSA of tested values: ", test_values)
    # dictionary_results[model_name] = [rmse_value, 'rnn']
    return prediction


def get_prediction_svm(model, xtest):
    prediction = model.predict(X=xtest)
    return prediction


def rmse(ytest, pred):
    return metrics.mean_squared_error(ytest, pred, squared=False)


five_yr_stock_sent = pd.read_csv("gen_data/fiveyr_stock_sentiment_full.csv")
intra_yr_stock_sent = pd.read_csv("gen_data/intra_stock_sentiment_full.csv")

five_yr_sent_full_with_senti = five_yr_stock_sent.reset_index(drop=True)
five_yr_sent_full_with_senti = five_yr_sent_full_with_senti.set_index(['tickerName', 'date', 'index'])

five_yr_sent_full_normalised = min_max_normilisation(five_yr_sent_full_with_senti)
train_5yr_senti_70, test_5yr_senti_30_normalised = split_dataset(five_yr_sent_full_normalised, 0.7, 2300)

# # normalise values
# min max normilisation was used as it produced the required values of between 0 - 1 unlike mean normalisation
five_yr_sent_full_normalised = min_max_normilisation(five_yr_sent_full_with_senti)

# split dataset
train_5yr_senti_70, test_5yr_senti_30_normalised = split_dataset(five_yr_sent_full_normalised, 0.7, 2300)
train_5yr_senti_75, test_5yr_senti_25_normalised = split_dataset(five_yr_sent_full_normalised, 0.75, 2300)
train_5yr_senti_80, test_5yr_senti_20_normalised = split_dataset(five_yr_sent_full_normalised, 0.8, 2300)

randomized_5yr_full_dataset = five_yr_sent_full_normalised.sample(frac=1.0, random_state=2300)

# 70 - 30 train / test
xtest_5yr_senti_30, ytest_5yr_senti_30 = get_x_y_keras(test_5yr_senti_30_normalised, 'close')

# 75 - 25 train / test
xtest_5yr_senti_25, ytest_5yr_senti_25 = get_x_y_keras(test_5yr_senti_25_normalised, 'close')

# 80 - 20 train / test
xtest_5yr_senti_20, ytest_5yr_senti_20 = get_x_y_keras(test_5yr_senti_20_normalised, 'close')

five_yr_no_senti_full = five_yr_stock_sent.set_index(['tickerName', 'date', 'index'])

five_yr_no_senti_normalised = min_max_normilisation(five_yr_no_senti_full)
five_yr_no_senti_normalised = five_yr_no_senti_normalised.drop(axis=1,
                                                               labels=['title_pos_sentiment',
                                                                       'title_avg_pos_sent',
                                                                       'title_neg_sentiment',
                                                                       'title_avg_neg_sentiment',
                                                                       'title_sentiment_difference',
                                                                       'article_pos_sentiment',
                                                                       'article_avg_pos_sentiment',
                                                                       'article_neg_sentiment',
                                                                       'article_avg_neg_sentiment',
                                                                       'article_sentiment_difference'])
# split dataset
train_no_senti_70, test_no_senti_30 = split_dataset(five_yr_no_senti_normalised, 0.7, 2300)
train_no_senti_75, test_no_senti_25 = split_dataset(five_yr_no_senti_normalised, 0.75, 2300)
train_no_senti_80, test_no_senti_20 = split_dataset(five_yr_no_senti_normalised, 0.8, 2300)
randomized_5yr_no_senti_dataset = five_yr_no_senti_normalised.sample(frac=1.0, random_state=2300)

# 70 - 30 train / test
xtest_no_senti_30, ytest_no_senti_30 = get_x_y_keras(test_no_senti_30, 'close')

# 75 - 25 train / test
xtest_no_senti_25, ytest_no_senti_25 = get_x_y_keras(test_no_senti_25, 'close')

xtest_no_senti_20, ytest_no_senti_20 = get_x_y_keras(test_no_senti_20, 'close')
# 80 - 20 train / test


intra_stock_sent = intra_yr_stock_sent.reset_index(drop=True)
intra_sent_full = intra_yr_stock_sent.set_index(['tickerName', 'date', 'minute', 'index'])

# # normalise values
# min max normilisation was used as it produced the required values of between 0 - 1 unlike mean normalisation
intra_sent_full_normilisation = min_max_normilisation(intra_sent_full)

# split dataset
train_intra_senti_70_normalised, test_intra_senti_30_normalised = split_dataset(intra_sent_full_normilisation, 0.7, 2300)
train_intra_senti_75_normalised, test_intra_senti_25_normalised = split_dataset(intra_sent_full_normilisation, 0.75, 2300)
train_intra_senti_80_normalised, test_intra_senti_20_normalised = split_dataset(intra_sent_full_normilisation, 0.8, 2300)
randomized_intra_senti_dataset = intra_sent_full_normilisation.sample(frac=1.0, random_state=2300)

# 70 - 30 train / test
xtest_intra_senti_30, ytest_intra_senti_30 = get_x_y_keras(test_intra_senti_30_normalised, 'close')

# 75 - 25 train / test
xtest_intra_senti_25, ytest_intra_senti_25 = get_x_y_keras(test_intra_senti_25_normalised, 'close')

# 80 - 20 train / test
xtest_intra_senti_20, ytest_intra_senti_20 = get_x_y_keras(test_intra_senti_20_normalised, 'close')

# ----------------------------------------------
#             INTRA NO SENTIMENT
# ----------------------------------------------
intra_sent_full = intra_yr_stock_sent.set_index(['tickerName', 'date', 'minute', 'index'])

intra_no_senti_normalised = min_max_normilisation(intra_sent_full)
intra_no_senti_normalised = intra_no_senti_normalised.drop(axis=1,
                                                           labels=['title_pos_senti',
                                                                   'title_avg_pos_sent',
                                                                   'title_neg_senti',
                                                                   'title_avg_neg_sentiment',
                                                                   'title_senti_difference',
                                                                   'article_pos_senti',
                                                                   'article_avg_pos_sentiment',
                                                                   'article_neg_senti',
                                                                   'article_avg_neg_sentiment',
                                                                   'article_senti_difference'])

# split dataset
train_intra_no_senti_70_normalised, test_intra_no_senti_30_normalised = split_dataset(intra_no_senti_normalised, 0.7, 2300)
train_intra_no_senti_75_normalised, test_intra_no_senti_25_normalised = split_dataset(intra_no_senti_normalised, 0.75, 2300)
train_intra_no_senti_80_normalised, test_intra_no_senti_20_normalised = split_dataset(intra_no_senti_normalised, 0.8, 2300)
randomized_intra_no_senti_dataset = intra_no_senti_normalised.sample(frac=1.0, random_state=2300)

# 70 - 30 train / test
xtest_intra_no_senti_30, ytest_intra_no_senti_30 = get_x_y_keras(test_intra_no_senti_30_normalised, 'close')

# 75 - 25 train / test
xtest_intra_no_senti_25, ytest_intra_no_senti_25 = get_x_y_keras(test_intra_no_senti_25_normalised, 'close')

# 80 - 20 train / test
xtest_intra_no_senti_20, ytest_intra_no_senti_20 = get_x_y_keras(test_intra_no_senti_20_normalised, 'close')

# Strongest Models
model_rnn_5yr_full_70_30 = keras_load_model("gen_data\\rnn_models\\model_rnn_5yr_full_70_30.h5")
model_rnn_5yr_full_80_20 = keras_load_model("gen_data\\rnn_models\\model_rnn_5yr_full_80_20.h5")
model_rnn_5yr_no_senti_75_25 = keras_load_model("gen_data\\rnn_models\\model_rnn_5yr_no_senti_75_25.h5")
model_rnn_5yr_no_senti_80_20 = keras_load_model("gen_data\\rnn_models\\model_rnn_5yr_no_senti_80_20.h5")
model_cnn_relu_relu_5yr_no_senti_70_30 = keras_load_model("gen_data\\cnn_models\\model_cnn_relu_relu_5yr_no_senti_70_30.h5")

# Weakest Models
model_cnn_sigmoid_relu_intra_senti_70_30 = keras_load_model("gen_data\\cnn_models\\model_cnn_sigmoid_relu_intra_senti_70_30.h5")
model_cnn_sigmoid_relu_no_intra_senti_70_30 = keras_load_model("gen_data\\cnn_models\\model_cnn_sigmoid_relu_no_intra_senti_70_30.h5")
model_cnn_relu_sigmoid_intra_senti_80_20 = keras_load_model("gen_data\\cnn_models\\model_cnn_relu_sigmoid_intra_senti_80_20.h5")
model_cnn_sigmoid_relu_intra_no_senti_80_20 = keras_load_model("gen_data\\cnn_models\\model_cnn_sigmoid_relu_intra_no_senti_80_20.h5")
svm_regression_model_intra_no_senti_75_25 = load("gen_data\\svm_models\\svm_regression_model_intra_no_senti_75_25.joblib")


def export_pred_value(model, xtest, test_dataframe, ytest, file_path, model_name):
    if "svm" in model_name:
        predicted_values = get_prediction_svm(model, xtest)
    else:
        predicted_values = get_prediciton_keras(model, xtest)
    predDataset = test_dataframe
    predDataset['normalised_closed'] = ytest
    predDataset['predicted_close'] = predicted_values
    predDataset = reverse_min_max_normilisation(predDataset)
    predDataset['predicted_diff'] = predDataset['close'] - predDataset['denormalised_pred_close']
    predDataset = predDataset.reset_index()
    predDataset.plot(y=['close', 'denormalised_pred_close'], x='date', title=f"{model_name}")
    predDataset.to_csv(f"{file_path}//{model_name}.csv",
                       index=True, header=True, mode="w", sep=',')


do_not_use_5yr_70, test_5yr_30 = split_dataset(five_yr_stock_sent, 0.70, 2300)
do_not_use_5yr_75, test_5yr_25 = split_dataset(five_yr_stock_sent, 0.75, 2300)
do_not_use_5yr_80, test_5yr_20 = split_dataset(five_yr_stock_sent, 0.80, 2300)

intra_yr_stock_sent_no_na = intra_yr_stock_sent.dropna()
do_not_use_intra_70, test_intra_30 = split_dataset(intra_yr_stock_sent_no_na, 0.70, 2300)
do_not_use_intra_75, test_intra_25 = split_dataset(intra_yr_stock_sent_no_na, 0.75, 2300)
do_not_use_intra_80, test_intra_20 = split_dataset(intra_yr_stock_sent_no_na, 0.80, 2300)

svm_xtest_intra_no_senti_25, svm_ytest_intra_no_senti_25 = get_x_y(test_intra_no_senti_25_normalised, 'close')

# Strongest 5
export_pred_value(model_rnn_5yr_full_70_30, xtest_5yr_senti_30, test_5yr_30, ytest_5yr_senti_30, "gen_data//topresults", "1_model_rnn_5yr_full_70_30")
export_pred_value(model_rnn_5yr_full_80_20, xtest_5yr_senti_20, test_5yr_20, ytest_5yr_senti_20, "gen_data//topresults", "2_model_rnn_5yr_full_80_20")
export_pred_value(model_rnn_5yr_no_senti_75_25, xtest_no_senti_25, test_5yr_25, ytest_no_senti_25, "gen_data//topresults", "3_model_rnn_5yr_no_senti_75_25")
export_pred_value(model_rnn_5yr_no_senti_80_20, xtest_no_senti_20, test_5yr_20, ytest_no_senti_20, "gen_data//topresults", "4_model_rnn_5yr_no_senti_80_20")
export_pred_value(model_cnn_relu_relu_5yr_no_senti_70_30, xtest_no_senti_30, test_5yr_30, ytest_no_senti_30, "gen_data//topresults", "5_model_cnn_relu_relu_5yr_no_senti_70_30")

# Weakest 5
export_pred_value(model_cnn_sigmoid_relu_intra_senti_70_30, xtest_intra_senti_30, test_intra_30, ytest_intra_senti_30, "gen_data//lowestresults", "1_model_cnn_sigmoid_relu_intra_senti_70_30")
export_pred_value(model_cnn_sigmoid_relu_no_intra_senti_70_30, xtest_intra_no_senti_30, test_intra_30, ytest_intra_no_senti_30, "gen_data//lowestresults", "2_model_cnn_sigmoid_relu_no_intra_senti_70_30")
export_pred_value(model_cnn_relu_sigmoid_intra_senti_80_20, xtest_intra_senti_20, test_intra_20, ytest_intra_senti_20, "gen_data//lowestresults", "3_model_cnn_relu_sigmoid_intra_senti_80_20")
export_pred_value(model_cnn_sigmoid_relu_intra_no_senti_80_20, xtest_intra_no_senti_20, test_intra_20, ytest_intra_no_senti_20, "gen_data//lowestresults", "4_model_cnn_sigmoid_relu_intra_no_senti_80_20")
export_pred_value(svm_regression_model_intra_no_senti_75_25, svm_xtest_intra_no_senti_25, test_intra_25, svm_ytest_intra_no_senti_25, "gen_data//lowestresults", "5_svm_regression_model_intra_no_senti_75_25")

plot.show()

# x,y =split_dataset(five_yr_stock_sent,0.7,2300)
# y = y.set_index(['tickerName', 'date', 'index'])
# y['predicted_close'] = 0.0
# y['predicted_diff'] = 0.0
# reversed = reverse_min_max_normilisation(test_5yr_senti_30_pred_datasset,y)

# shape_of_pred = prediction.shape

# prediction = prediction.reshape(shape_of_pred[0])
# prediction = pd.Series(prediction)
# prediction = pd.DataFrame(data=prediction,columns=['closed prediciton'])
# data = xtest_5yr_senti_30.copy()
# data = data.insert(10,'close',ytest_5yr_senti_30,True)
# data = data.insert(11,'close prediction',prediction,True)
# data = pd.concat((data, ytest_5yr_senti_30, prediction), axis=1)
# print(data)

# print(test_5yr_senti_30.head())
