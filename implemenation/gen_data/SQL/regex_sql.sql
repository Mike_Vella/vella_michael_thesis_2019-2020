#Reuters 
update thesisdb.news_articles_no_extra_string_timestmp
set `timestamp` = REGEXP_REPLACE(timestamp, "([aA-zZ]+) ([0-9]+), ([0-9]+) ([0-9]+:[0-9]+[a-z]+ [A-Z]+)", "$3-$1-$2")
where source = 'Reuters';

update thesisdb.news_articles_no_extra_string_timestmp
set `timestamp` = REGEXP_REPLACE(timestamp, "([aA-zZ]+) ([0-9]+), ([0-9]+)", "$3-$1-$2") where source = 'Bloomberg';

update news_articles_no_extra_string_timestmp
set timestamp = regexp_replace(REGEXP_SUBSTR(timestamp, "([aA-zZ]+ [aA-zZ]+, [aA-zZ]+ \|)( ([aA-zZ]+) ([0-9]+), ([0-9]+))"), "([aA-zZ]+) ([0-9]+), ([0-9]+)", "$3-$1-$2")
where source = 'The Motley Fool';

select * FROM thesisdb.news_articles_no_extra_string_timestmp where timestamp is null;