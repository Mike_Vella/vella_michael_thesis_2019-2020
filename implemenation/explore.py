import pandas as pd
import datetime
import matplotlib.pyplot as plot

pd.set_option('display.max_columns', None)
pd.set_option('display.expand_frame_repr', False)
pd.set_option('max_colwidth', None)

fiveyr_stage = pd.read_csv("C:\\vella_michael_thesis_2019-2020\\implemenation\\gen_data\\5yr_stage_export.csv")


# aapl_fiveyr = fiveyr_stage[fiveyr_stage['tickerName'].str.contains('aapl')]
# aapl_fiveyr = aapl_fiveyr.reset_index()
# aapl_fiveyr.set_index('date')
#
# print("Lowest close\n\n", aapl_fiveyr[aapl_fiveyr['close'] == aapl_fiveyr['close'].min()])
# print("\n\nhighest close\n\n", aapl_fiveyr[aapl_fiveyr['close'] == aapl_fiveyr['close'].max()])
# print("\n\nAverage AAPL share at close over 5 yrs: ", aapl_fiveyr['close'].mean())
#
#
# atvi_fiveyr = fiveyr_stage[fiveyr_stage['tickerName'].str.contains('atvi')]
# atvi_fiveyr = atvi_fiveyr.reset_index()
# atvi_fiveyr.set_index('date')
#
# print("ATVI lowest close\n\n", atvi_fiveyr[atvi_fiveyr['close'] == atvi_fiveyr['close'].min()])
# print("\n\nATVI highest close\n\n", atvi_fiveyr[aapl_fiveyr['close'] == aapl_fiveyr['close'].max()])
#
#
# print("\n\nAverage ATVI share at close over 5 yrs: ", atvi_fiveyr['close'].mean())
#
#
# atvi_fiveyr = fiveyr_stage[fiveyr_stage['tickerName'].str.contains('atvi')]
# atvi_fiveyr = atvi_fiveyr.reset_index()
# atvi_fiveyr.set_index('date')
#
# print("ATVI lowest close\n\n", atvi_fiveyr[atvi_fiveyr['close'] == atvi_fiveyr['close'].min()])
# print("\n\nATVI highest close\n\n", atvi_fiveyr[aapl_fiveyr['close'] == aapl_fiveyr['close'].max()])
#
#
# print("\n\nAverage ATVI share at close over 5 yrs: ", atvi_fiveyr['close'].mean())
#
#
# aapl_fiveyr.plot(y=['close', 'open'], x='date', title="AAPL 5yr Close & Open")
# atvi_fiveyr.plot(y=['close', 'open'], x='date', title="ATVI 5yr Close & Open")


def show_some_stats(ticker_list: [str]):
    for ticker in ticker_list:
        ticker_fiveyr = fiveyr_stage[fiveyr_stage['tickerName'].str.contains(ticker)]
        ticker_fiveyr = ticker_fiveyr.reset_index()
        ticker_fiveyr.set_index('date')

        print(ticker.capitalize(), "lowest close\n\n",
              ticker_fiveyr[ticker_fiveyr['close'] == ticker_fiveyr['close'].min()])
        print("\n\n", ticker.capitalize(), "highest close\n\n",
              ticker_fiveyr[ticker_fiveyr['close'] == ticker_fiveyr['close'].max()])

        print("\n\nAverage ", ticker.capitalize(), " share at close over 5 yrs: ", ticker_fiveyr['close'].mean())

        ticker_fiveyr.plot(y=['close', 'open'], x='date', title=F"{ticker.capitalize()} 5yr Close & Open")


tckr_lst = ['aapl', 'xom', 'pep', 'nvda', 'viaca','viac', 'jcp', 'wu', 'atvi', 'hog']
show_some_stats(tckr_lst)
plot.show()
