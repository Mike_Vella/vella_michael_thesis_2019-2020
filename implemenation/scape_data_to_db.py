import time

import requests

from requests.exceptions import RequestException
from contextlib import closing
from bs4 import BeautifulSoup
import pandas as panda
import urllib3
import os
from datetime import datetime
import re
from collections import defaultdict
from sqlalchemy import create_engine
import scrapy

engine = create_engine("mysql+pymysql://{user}:{pw}@localhost/{db}"
                       .format(user="root",
                               pw="rootpassword",
                               db="thesisdb"))


# Based on the following tutorial https://realpython.com/python-web-scraping-practical-introduction/

def is_good_response(resp):
    """
    :param resp:
    :return: True if response seems to be HTML, false otherwise
    """
    content_type = resp.headers['Content-Type'].lower()
    return (resp.status_code == 200
            and content_type is not True
            and content_type.find('html') > -1)


def log_error(e):
    """
    :param e: string version of reported error
    :return: prints string error
    """
    print(e)


def simple_get(url):
    """Attempts to get the content at 'url' by making an HTTP Get request.
       If the content type is pf some kind of HTML/XML, return the text content
       else return NONE
       """
    try:
        with closing(requests.get(url, stream=True,proxies={"http":"107.150.48.126"}, headers={
            'accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8',
            'accept-language': 'en-US,en;q=0.9,fr;q=0.8,ro;q=0.7,ru;q=0.6,la;q=0.5,pt;q=0.4,de;q=0.3',
            'cache-control': 'max-age=0',
            'upgrade-insecure-requests': '1',
            'user-agent':  	'Mozilla/5.0 (Windows NT x.y; rv:10.0) Gecko/20100101 Firefox/10.0'
        })) as resp:
            if is_good_response(resp):
                return resp.content
            else:
                return None

    except RequestException as e:
        log_error('Error during request to {0} : {1}'.format(url, str(e)))


def get_curr_ticker(filename, dir):
    ticker = filename.replace(dir, "")
    ticker = ticker.replace(".html", "")
    ticker = ticker.replace(" ", "")
    print("Current Ticker: " + ticker)
    return ticker


list_dir = ['Reuters', 'The Motley Fool', 'Bloomberg']
folder_path = "C:\\vella_michael_thesis_2019-2020\implemenation\gen_data\data_scraping\\"

failed_links = []

# full_filepath = "C:\\vella_michael_thesis_2019-2020\implemenation\gen_data\data_scraping\Reuters\Reuters NVDA.html"
# soup = BeautifulSoup(open(full_filepath, encoding="utf8"), features="html5lib")
# scrapedResults = soup.findAll(attrs={"class": "search-result-content"})

# reuters_site = 'https://www.reuters.com'
# ticker = get_curr_ticker(full_filepath, "Reuters")
# for scrapedResult in scrapedResults:
#     anchor = scrapedResult.find('a')
#     timestamp = scrapedResult.find(attrs={"class": "search-result-timestamp"}).get_text()
#     link = reuters_site + anchor.get('href')
#     html_document = simple_get(link)
#
#     if html_document is not None and html_document.__len__() > 0:
#         html = BeautifulSoup(html_document, 'html5lib')
#         title = html.find(attrs={'class': 'ArticleHeader_content-container'})
#         articles = html.find(attrs={'class': 'StandardArticleBody_body'})
#         full_article = ""
#         actual_title = ""
#         try:
#             if title is not None and articles is not None:
#
#                 for article in articles:
#                     article.get('p')
#                     full_article = F"{full_article} {article.get_text()}"
#
#                 for tlt in title:
#                     tlt.get('h3')
#                     actual_title = tlt.get_text()
#
#             print("Link:" + link)
#             print("Timestamp: " + timestamp)
#             print("Title: " + actual_title)
#             print("Article: " + full_article)
#
#             source = dir
#
#             dict['link'].append(link)
#             dict['ticker'].append(ticker)
#             dict['timestamp'].append(timestamp)
#             dict['title'].append(actual_title)
#             dict['article'].append(full_article)
#             dict['source'].append(source)
#
#             requests.sessions.session().close()
#             print()
#
#         except:
#             failed_links.append(link)
#             print("Something went wrong when retrieving the article")

for dir in list_dir:
    dir_path = folder_path + dir
    for file in os.listdir(dir_path):
        if file.endswith(".html"):
            if dir == "Reuters":
                full_filepath = F"{dir_path}\\{file}"
                soup = BeautifulSoup(open(full_filepath, encoding="utf8"), features="html5lib")
                scrapedResults = soup.findAll(attrs={"class": "search-result-content"})

                reuters_site = 'https://www.reuters.com'
                ticker = get_curr_ticker(file, dir)
                for scrapedResult in scrapedResults:
                    anchor = scrapedResult.find('a')
                    timestamp = scrapedResult.find(attrs={"class": "search-result-timestamp"}).get_text()
                    link = reuters_site + anchor.get('href')
                    html_document = simple_get(link)

                    if html_document is not None and html_document.__len__() > 0:
                        html = BeautifulSoup(html_document, 'html5lib')
                        title = html.find(attrs={'class': 'ArticleHeader_content-container'})
                        articles = html.find(attrs={'class': 'StandardArticleBody_body'})
                        full_article = ""
                        actual_title = ""
                        try:
                            if title is not None and articles is not None:
                                for article in articles:
                                    article.get('p')
                                    full_article = F"{full_article} {article.get_text()}"

                                for tlt in title:
                                    tlt.get('h3')
                                    actual_title = tlt.get_text()

                                source = dir

                                print("Ticker: " + ticker)
                                print("Link:" + link)
                                print("Timestamp: " + timestamp)
                                print("Title: " + actual_title)
                                print("Article: " + full_article)
                                print("Source: " + source)

                                dict['link'].append(link)
                                dict['ticker'].append(ticker)
                                dict['timestamp'].append(timestamp)
                                dict['title'].append(actual_title)
                                dict['article'].append(full_article)
                                dict['source'].append(source)

                                requests.sessions.session().close()
                                print()
                        except:
                            failed_links.append(link)
                            print("Something went wrong")
                            requests.sessions.session().close()

            if dir == "Bloomberg":
                full_filepath = F"{dir_path}\\{file}"
                soup = BeautifulSoup(open(full_filepath, encoding="utf8"), features="html5lib")
                scrapedResults = soup.findAll(attrs={"class": "storyItem__192ee8af"})
                ticker = get_curr_ticker(file, dir)
                for scrapedResult in scrapedResults:
                    anchor = scrapedResult.find('a')
                    timestamp = scrapedResult.find(attrs={"class": "publishedAt__79f8aaad"}).get_text()
                    link = anchor.get('href')
                    time.sleep(20)
                    html_document = simple_get(link)

                    if html_document is not None and html_document.__len__() > 0:
                        html = BeautifulSoup(html_document, 'html5lib')
                        title = html.find(attrs={'class': 'lede-text-v2__container'})
                        articles = html.find(attrs={'class': 'body-copy-v2 fence-body'})
                        full_article = ""
                        actual_title = ""

                        if title is not None and articles is not None:
                            try:
                                for article in articles.find_all_next("p"):
                                    # article = article.find('p')
                                    full_article = F"{full_article} {article.get_text()}"

                                for tlt in title.find_all_next("h1"):
                                    actual_title = tlt.get_text()

                                source = dir

                                print("Ticker: " + ticker)
                                print("Link:" + link)
                                print("Timestamp: " + timestamp)
                                print("Title: " + actual_title)
                                print("Article: " + full_article)
                                print("Source: " + source)

                                dict = defaultdict(list)

                                dict['link'].append(link)
                                dict['ticker'].append(ticker)
                                dict['timestamp'].append(timestamp)
                                dict['title'].append(actual_title)
                                dict['article'].append(full_article)
                                dict['source'].append(source)

                                requests.sessions.session().close()

                                dataframe = panda.DataFrame(data=dict,
                                                            columns=['link', 'ticker', 'timestamp', "title", "article",
                                                                     "source"])
                                dataframe.to_sql('news_articles', con=engine, if_exists='append')
                                dict.clear()
                                print()
                            except:
                                print("Something went wrong")
                                requests.sessions.session().close()
                                failed_links.append(link)

            if dir == "The Motley Fool":
                the_fool_site = "https://www.fool.com"
                full_filepath = F"{dir_path}\\{file}"
                soup = BeautifulSoup(open(full_filepath, encoding="utf8"), features="html5lib")
                scrapedResults = soup.findAll(attrs={"class": "text"})
                ticker = get_curr_ticker(file, dir)
                for scrapedResult in scrapedResults:
                    anchor = scrapedResult.find('a')
                    timestamp = scrapedResult.find(attrs={"class": "story-date-author"}).get_text()
                    link = the_fool_site + anchor.get('href')
                    html_document = simple_get(link)

                    if html_document is not None and html_document.__len__() > 0:
                        html = BeautifulSoup(html_document, 'html5lib')
                        title = html.find(attrs={'class': 'usmf-new article-header'})
                        articles = html.find(attrs={'class': 'article-content'})
                        full_article = ""
                        actual_title = ""

                        if title is not None and articles is not None:
                            try:
                                for article in articles.find_all_next('p'):
                                    full_article = F"{full_article} {article.get_text()}"

                                for tlt in title.find_all_next('h1'):
                                    tlt.get('h1')
                                    actual_title = tlt.get_text()

                                source = dir

                                print("Ticker: " + ticker)
                                print("Link:" + link)
                                print("Timestamp: " + timestamp)
                                print("Title: " + actual_title)
                                print("Article: " + full_article)
                                print("Source: " + source)

                                dict['link'].append(link)
                                dict['ticker'].append(ticker)
                                dict['timestamp'].append(timestamp)
                                dict['title'].append(actual_title)
                                dict['article'].append(full_article)
                                dict['source'].append(source)

                                requests.sessions.session().close()
                                print()
                            except:
                                requests.sessions.session().close()
                                failed_links.append(link)
                                print("Something went wrong")



print(dataframe)


if failed_links.__len__() > 0:
    with open("C:\\vella_michael_thesis_2019-2020\\implemenation\\gen_data\\failed_links.txt", "w") as out_file:
        for row in failed_links:
            out_file.write(str(row) + '\n')
