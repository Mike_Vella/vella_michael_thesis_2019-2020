import pandas
import os
from sqlalchemy import create_engine


def set_engine(user="root", password="rootpassword", db=str):
    engine = create_engine("mysql+pymysql://{user}:{pw}@localhost/{db}"
                           .format(user=user,
                                   pw=password,
                                   db=db))
    return engine


def merge_dataset(data_dir=str, file_name=str, output_dir=str):
    '''
        Merges all CSVs found in the given directory

        Based on this stack overflow answer: https://stackoverflow.com/questions/48051100/python-pandas-merge-multiple-csv-files

        :param data_dir:the given directory
        :param file_name: output file name
        :return: merged dataframe
    '''
    os.chdir(data_dir)
    merger = [pandas.read_csv(data_file, index_col=[0], skipinitialspace=True)
              for data_file in os.listdir(os.getcwd()) if data_file.endswith('csv')]
    merged = pandas.concat(merger, axis=0, join='inner')
    print(merged)
    os.chdir(output_dir)
    merged.to_csv(f'{file_name}.csv')
    return merged


intra_db = set_engine(db="thesisdb")
intra_merge = pandas.DataFrame(merge_dataset('../gen_data/intraday_dataset', 'intraday_merger', '..'))
intra_merge = intra_merge.reset_index()
intra_merge.insert(0,column='intra_DateTime',value=intra_merge['tickerName'] + '_' +
                                                   intra_merge['tickerDate'].astype(str) + '_' +
                                                   intra_merge['minute'].astype(str))
intra_merge.to_sql('intra_stage', con=intra_db, if_exists='append')

#
# fiveyr_db = set_engine(db="thesisdb")
# fiveyr_merge = pandas.DataFrame(merge_dataset('../gen_data/5yr_dataset', '5yr_merger', '..'))
# fiveyr_merge = fiveyr_merge.reset_index()
# fiveyr_merge.insert(0, 'fiveyr_Date', value=fiveyr_merge['tickerName'] + '_' + fiveyr_merge['date'].astype(str))
# fiveyr_merge.to_sql('fiveyr_stage', con=intra_db, if_exists='append')
