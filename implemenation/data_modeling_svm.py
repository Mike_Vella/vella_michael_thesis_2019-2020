import pandas as pd
import sqlalchemy
from sklearn.svm import SVR
from sklearn.model_selection import GridSearchCV
import sklearn.metrics as metrics
from joblib import dump

pd.set_option('display.max_columns', None)
pd.set_option('display.expand_frame_repr', False)
pd.set_option('max_colwidth', None)


def min_max_normilisation(dataframe):
    dataframe_normalised = (dataframe - dataframe.min()) / (dataframe.max() - dataframe.min())

    dataframe_normalised = dataframe_normalised.dropna()
    return dataframe_normalised


def split_dataset(dataset, test_size, seed):
    train = dataset.sample(frac=test_size, random_state=seed)
    # sampling removes the data present in the dataframe
    # Drops columns that contain the training data, and samples the full remainder of data.
    test = dataset.drop(train.index).sample(frac=1.0)
    return train, test


def get_x_y(dataframe, yColumn):
    y = dataframe[f'{yColumn}']
    x = dataframe.drop(axis=1, labels=f'{yColumn}')

    xnumpy = pd.DataFrame.to_numpy(x)
    ynumpy = pd.DataFrame.to_numpy(y)
    return xnumpy, ynumpy


def run_regression(xtrain, ytrain, xtest, ytest, hyperparams, cv, verbose, threads, model_name):
    model = GridSearchCV(estimator=SVR(), param_grid=hyperparams, cv=cv, verbose=verbose, n_jobs=threads)
    print(f"Starting fitting for model {model_name}")
    model = model.fit(xtrain, ytrain)
    print(f"Starting prediction for model {model_name}")
    prediction = model.predict(X=xtest)
    print(f"RSME value {metrics.mean_squared_error(y_true=ytest, y_pred=prediction, squared=False)} for model "
          f"{model_name}")
    print(f"dumping {model_name}")
    dump(model, f'{model_name}.joblib')


# importing data for both fiveyr and intra
five_yr_stock_sent = pd.read_csv("gen_data/fiveyr_stock_sentiment_full.csv")
intra_yr_stock_sent = pd.read_csv("gen_data/intra_stock_sentiment_full.csv")

# ----------------------------------------------
#             FIVE YR WITH SENTIMENT
# ----------------------------------------------
five_yr_stock_sent = five_yr_stock_sent.reset_index(drop=True)
five_yr_sent_full_with_senti = five_yr_stock_sent.set_index(['tickerName', 'date', 'index'])

# # normalise values
# min max normilisation was used as it produced the required values of between 0 - 1 unlike mean normalisation
five_yr_sent_full_normalised = min_max_normilisation(five_yr_sent_full_with_senti)

# split dataset
train_5yr_senti_70, test_5yr_senti_30 = split_dataset(five_yr_sent_full_normalised, 0.7, 2300)
train_5yr_senti_75, test_5yr_senti_25 = split_dataset(five_yr_sent_full_normalised, 0.75, 2300)
train_5yr_senti_80, test_5yr_senti_20 = split_dataset(five_yr_sent_full_normalised, 0.8, 2300)

# 70 - 30 train / test
xtrain_5yr_senti_70, ytrain_5yr_senti_70 = get_x_y(train_5yr_senti_70, 'close')
xtest_5yr_senti_30, ytest_5yr_senti_30 = get_x_y(test_5yr_senti_30, 'close')

# 75 - 25 train / test
xtrain_5yr_senti_75, ytrain_5yr_senti_75 = get_x_y(train_5yr_senti_75, 'close')
xtest_5yr_senti_25, ytest_5yr_senti_25 = get_x_y(test_5yr_senti_25, 'close')

# 80 - 20 train / test
xtrain_5yr_senti_80, ytrain_5yr_senti_80 = get_x_y(train_5yr_senti_80, 'close')
xtest_5yr_senti_20, ytest_5yr_senti_20 = get_x_y(test_5yr_senti_20, 'close')

# ----------------------------------------------
#             FIVE YR NO SENTIMENT
# ----------------------------------------------
five_yr_no_senti_full = five_yr_stock_sent.set_index(['tickerName', 'date', 'index'])

five_yr_no_senti_normalised = min_max_normilisation(five_yr_no_senti_full)
five_yr_no_senti_normalised = five_yr_no_senti_normalised.drop(axis=1,
                                                               labels=['title_pos_sentiment',
                                                                       'title_avg_pos_sent',
                                                                       'title_neg_sentiment',
                                                                       'title_avg_neg_sentiment',
                                                                       'title_sentiment_difference',
                                                                       'article_pos_sentiment',
                                                                       'article_avg_pos_sentiment',
                                                                       'article_neg_sentiment',
                                                                       'article_avg_neg_sentiment',
                                                                       'article_sentiment_difference'])
# split dataset
train_no_senti_70, test_no_senti_30 = split_dataset(five_yr_no_senti_normalised, 0.7, 2300)
train_no_senti_75, test_no_senti_25 = split_dataset(five_yr_no_senti_normalised, 0.75, 2300)
train_no_senti_80, test_no_senti_20 = split_dataset(five_yr_no_senti_normalised, 0.8, 2300)

# 70 - 30 train / test
xtrain_no_senti_70, ytrain_no_senti_70 = get_x_y(train_no_senti_70, 'close')
xtest_no_senti_30, ytest_no_senti_30 = get_x_y(test_no_senti_30, 'close')

# 75 - 25 train / test
xtrain_no_senti_75, ytrain_no_senti_75 = get_x_y(train_no_senti_75, 'close')
xtest_no_senti_25, ytest_no_senti_25 = get_x_y(test_no_senti_25, 'close')

# 80 - 20 train / test
xtrain_no_senti_80, ytrain_no_senti_80 = get_x_y(train_no_senti_80, 'close')
xtest_no_senti_20, ytest_no_senti_20 = get_x_y(test_no_senti_20, 'close')

# ----------------------------------------------
#             INTRA WITH SENTIMENT
# ----------------------------------------------
intra_stock_sent = intra_yr_stock_sent.reset_index(drop=True)
intra_sent_full = intra_yr_stock_sent.set_index(['tickerName', 'date', 'minute', 'index'])

# # normalise values
# min max normilisation was used as it produced the required values of between 0 - 1 unlike mean normalisation
intra_sent_full_normilisation = min_max_normilisation(intra_sent_full)

# split dataset
train_intra_senti_70, test_intra_senti_30 = split_dataset(intra_sent_full_normilisation, 0.7, 2300)
train_intra_senti_75, test_intra_senti_25 = split_dataset(intra_sent_full_normilisation, 0.75, 2300)
train_intra_senti_80, test_intra_senti_20 = split_dataset(intra_sent_full_normilisation, 0.8, 2300)

# 70 - 30 train / test
xtrain_intra_senti_70, ytrain_intra_senti_70 = get_x_y(train_intra_senti_70, 'close')
xtest_intra_senti_30, ytest_intra_senti_30 = get_x_y(test_intra_senti_30, 'close')

# 75 - 25 train / test
xtrain_intra_senti_75, ytrain_intra_senti_75 = get_x_y(train_intra_senti_75, 'close')
xtest_intra_senti_25, ytest_intra_senti_25 = get_x_y(test_intra_senti_25, 'close')

# 80 - 20 train / test
xtrain_intra_senti_80, ytrain_intra_senti_80 = get_x_y(train_intra_senti_80, 'close')
xtest_intra_senti_20, ytest_intra_senti_20 = get_x_y(test_intra_senti_20, 'close')

# ----------------------------------------------
#             INTRA NO SENTIMENT
# ----------------------------------------------
intra_sent_full = intra_yr_stock_sent.set_index(['tickerName', 'date', 'minute', 'index'])

intra_no_senti_normalised = min_max_normilisation(intra_sent_full)
intra_no_senti_normalised = intra_no_senti_normalised.drop(axis=1,
                                                               labels=['title_pos_senti',
                                                                       'title_avg_pos_sent',
                                                                       'title_neg_senti',
                                                                       'title_avg_neg_sentiment',
                                                                       'title_senti_difference',
                                                                       'article_pos_senti',
                                                                       'article_avg_pos_sentiment',
                                                                       'article_neg_senti',
                                                                       'article_avg_neg_sentiment',
                                                                       'article_senti_difference'])
# split dataset
train_intra_no_senti_70, test_intra_no_senti_30 = split_dataset(intra_no_senti_normalised, 0.7, 2300)
train_intra_no_senti_75, test_intra_no_senti_25 = split_dataset(intra_no_senti_normalised, 0.75, 2300)
train_intra_no_senti_80, test_intra_no_senti_20 = split_dataset(intra_no_senti_normalised, 0.8, 2300)

# 70 - 30 train / test
xtrain_intra_no_senti_70, ytrain_intra_no_senti_70 = get_x_y(train_intra_no_senti_70, 'close')
xtest_intra_no_senti_30, ytest_intra_no_senti_30 = get_x_y(test_intra_no_senti_30, 'close')

# 75 - 25 train / test
xtrain_intra_no_senti_75, ytrain_intra_no_senti_75 = get_x_y(train_intra_no_senti_75, 'close')
xtest_intra_no_senti_25, ytest_intra_no_senti_25 = get_x_y(test_intra_no_senti_25, 'close')

# 80 - 20 train / test
xtrain_intra_no_senti_80, ytrain_intra_no_senti_80 = get_x_y(train_intra_no_senti_80, 'close')
xtest_intra_no_senti_20, ytest_intra_no_senti_20 = get_x_y(test_intra_no_senti_20, 'close')

# ----------------------------------------------
#                SVM MODELLING
# ----------------------------------------------

# Config for SVR model
hyperparams = [{'kernel': ['linear', 'poly', 'rbf', 'sigmoid'], 'coef0': [0.1, 0.2, 0.3, 0.4],
                'tol': [0.1, 0.01, 0.001, 0.0001, 0.00001], 'C': [0.1, 0.01, 0.001, 0.0001, 0.00001],
                'epsilon': [0.1, 0.01]}]

# fiveyr with sentiment
run_regression(xtrain_5yr_senti_70, ytrain_5yr_senti_70, xtest_5yr_senti_30, ytest_5yr_senti_30, hyperparams, 10, 1, 24, 'svm_regression_model_5yr_senti_70_30')
run_regression(xtrain_5yr_senti_75, ytrain_5yr_senti_75, xtest_5yr_senti_25, ytest_5yr_senti_25, hyperparams, 10, 1, 24, 'svm_regression_model_5yr_senti_75_25')
run_regression(xtrain_5yr_senti_80, ytrain_5yr_senti_80, xtest_5yr_senti_20, ytest_5yr_senti_20, hyperparams, 10, 1, 24, 'svm_regression_model_5yr_senti_80_20')

# fiveyr no sentiment
run_regression(xtrain_no_senti_70, ytrain_no_senti_70, xtest_no_senti_30, ytest_no_senti_30, hyperparams, 10, 1, 24, 'svm_regression_model_5yr_no_senti_70_30')
run_regression(xtrain_no_senti_75, ytrain_no_senti_75, xtest_no_senti_25, ytest_no_senti_25, hyperparams, 10, 1, 24, 'svm_regression_model_5yr_no_senti_75_25')
run_regression(xtrain_no_senti_80, ytrain_no_senti_80, xtest_no_senti_20, ytest_no_senti_20, hyperparams, 10, 1, 24, 'svm_regression_model_5yr_no_senti_80_20')

# intra with sentiment
run_regression(xtrain_intra_senti_70, ytrain_intra_senti_70, xtest_intra_senti_30, ytest_intra_senti_30, hyperparams, 10, 1, 24, 'svm_regression_model_intra_senti_70_30')
run_regression(xtrain_intra_senti_75, ytrain_intra_senti_75, xtest_intra_senti_25, ytest_intra_senti_25, hyperparams, 10, 1, 24, 'svm_regression_model_intra_senti_75_25')
run_regression(xtrain_intra_senti_80, ytrain_intra_senti_80, xtest_intra_senti_20, ytest_intra_senti_20, hyperparams, 10, 1, 24, 'svm_regression_model_intra_senti_80_20')

# intra no sentiment
run_regression(xtrain_intra_no_senti_70, ytrain_intra_no_senti_70, xtest_intra_no_senti_30, ytest_intra_no_senti_30, hyperparams, 10, 1, 24, 'svm_regression_model_intra_no_senti_70_30')
run_regression(xtrain_intra_no_senti_75, ytrain_intra_no_senti_75, xtest_intra_no_senti_25, ytest_intra_no_senti_25, hyperparams, 10, 1, 24, 'svm_regression_model_intra_no_senti_75_25')
run_regression(xtrain_intra_no_senti_80, ytrain_intra_no_senti_80, xtest_intra_no_senti_20, ytest_intra_no_senti_20, hyperparams, 10, 1, 24, 'svm_regression_model_intra_no_senti_80_20')

# OLD CODE
# ytraina = train_70['close']
# xtraina= train_70.drop(axis=1, labels='close')
#
# xtraina = pd.DataFrame.to_numpy(xtraina)
# ytraina = pd.DataFrame.to_numpy(ytraina)

# xtest = xtest[~pd.isna(xtest).any(axis=1)]
# ytest = pd.DataFrame.to_numpy(ytest)
# xtest = pd.DataFrame.to_numpy(xtest)

# model_01 = svm.SVR(kernel=['linear', 'poly', 'rbf', 'sigmoid', 'precomputed'], coef0=0.0, 0.2, 0.3, 0.4,
#                    tol=[1, 0.01, 0.001, 0.0001, 0.00001], C=[1, 0.01, 0.001, 0.0001, 0.00001], epsilon=[0.01, 0.1],
#                    verbose=True)
# model_01.fit(xtrain, ytrain)
# model_01.predict((xtest,xtrain))
# model_01.score(x=(xtest,xtrain),y=ytest)

# model = GridSearchCV(estimator=SVR(), param_grid=hyperparams, cv=10, verbose=1, n_jobs=24)
# model = model.fit(xtrain, ytrain)
# prediction = model.predict(X=xtest)
# print(metrics.mean_squared_error(y_true=ytest, y_pred=prediction, squared=False))
# dump(model, 'svm_regression_model_70_30.joblib')
