import os as os

import pandas as pd
import sklearn.metrics as metrics
from joblib import load as svm_load
from sqlalchemy import create_engine

def rmse(ytest, pred):
    return metrics.mean_squared_error(ytest, pred, squared=False)


def min_max_normilisation(dataframe):
    dataframe_normalised = (dataframe - dataframe.min()) / (dataframe.max() - dataframe.min())

    dataframe_normalised = dataframe_normalised.dropna()
    return dataframe_normalised


def split_dataset(dataset, test_size, seed):
    train = dataset.sample(frac=test_size, random_state=seed)
    # sampling removes the data present in the dataframe
    # Drops columns that contain the training data, and samples the full remainder of data.
    test = dataset.drop(train.index).sample(frac=1.0)
    return train, test


def get_x_y_keras(dataframe, yColumn):
    y = dataframe[f'{yColumn}']
    x = dataframe.drop(axis=1, labels=f'{yColumn}')

    xnumpy = pd.DataFrame.to_numpy(x)
    ynumpy = pd.DataFrame.to_numpy(y)

    shape_of_x = xnumpy.shape
    xnumpy = xnumpy.reshape((shape_of_x[0], 1, shape_of_x[1]))

    return xnumpy, ynumpy


def get_x_y(dataframe, yColumn):
    y = dataframe[f'{yColumn}']
    x = dataframe.drop(axis=1, labels=f'{yColumn}')

    xnumpy = pd.DataFrame.to_numpy(x)
    ynumpy = pd.DataFrame.to_numpy(y)
    return xnumpy, ynumpy


def results_keras(xtest, ytest, file, model):
    '''

    :param xtest:
    :param ytest:
    :param file:
    :return: rmse value
    '''
    prediction = model.predict(xtest)
    rmse_value = rmse(ytest, prediction)
    print(f"Model:{file} \t || RMSE: {rmse_value}")
    return rmse_value


# importing data for both fiveyr and intra
five_yr_stock_sent = pd.read_csv("gen_data/fiveyr_stock_sentiment_full.csv")
intra_yr_stock_sent = pd.read_csv("gen_data/intra_stock_sentiment_full.csv")

# --------------------------------
#   5yr SENTIMENT PREPARATION
# --------------------------------

five_yr_sent_full_with_senti = five_yr_stock_sent.reset_index(drop=True)
five_yr_sent_full_with_senti = five_yr_sent_full_with_senti.set_index(['tickerName', 'date', 'index'])

five_yr_sent_full_normalised = min_max_normilisation(five_yr_sent_full_with_senti)

# split dataset
train_5yr_senti_70, test_5yr_senti_30 = split_dataset(five_yr_sent_full_normalised, 0.7, 2300)
train_5yr_senti_75, test_5yr_senti_25 = split_dataset(five_yr_sent_full_normalised, 0.75, 2300)
train_5yr_senti_80, test_5yr_senti_20 = split_dataset(five_yr_sent_full_normalised, 0.8, 2300)

xtest_5yr_senti_30, ytest_5yr_senti_30 = get_x_y(test_5yr_senti_30, 'close')
xtest_5yr_senti_25, ytest_5yr_senti_25 = get_x_y(test_5yr_senti_25, 'close')
xtest_5yr_senti_20, ytest_5yr_senti_20 = get_x_y(test_5yr_senti_20, 'close')

# --------------------------------
#   5yr NO SENTIMENT PREPARATION
# --------------------------------

five_yr_no_senti_full = five_yr_stock_sent.set_index(['tickerName', 'date', 'index'])

five_yr_no_senti_normalised = min_max_normilisation(five_yr_no_senti_full)
five_yr_no_senti_normalised = five_yr_no_senti_normalised.drop(axis=1,
                                                               labels=['title_pos_sentiment', 'title_avg_pos_sent',
                                                                       'title_neg_sentiment', 'title_avg_neg_sentiment',
                                                                       'title_sentiment_difference',
                                                                       'article_pos_sentiment',
                                                                       'article_avg_pos_sentiment',
                                                                       'article_neg_sentiment',
                                                                       'article_avg_neg_sentiment',
                                                                       'article_sentiment_difference'])
# split dataset
train_no_senti_70, test_no_senti_30 = split_dataset(five_yr_no_senti_normalised, 0.7, 2300)
train_no_senti_75, test_no_senti_25 = split_dataset(five_yr_no_senti_normalised, 0.75, 2300)
train_no_senti_80, test_no_senti_20 = split_dataset(five_yr_no_senti_normalised, 0.8, 2300)

xtest_5yr_no_senti_30, ytest_5yr_no_senti_30 = get_x_y(test_no_senti_30, 'close')
xtest_5yr_no_senti_25, ytest_5yr_no_senti_25 = get_x_y(test_no_senti_25, 'close')
xtest_5yr_no_senti_20, ytest_5yr_no_senti_20 = get_x_y(test_no_senti_20, 'close')

# ------------------------
#      INTRA SENTIMENT
# ------------------------

intra_stock_sent = intra_yr_stock_sent.reset_index(drop=True)
intra_sent_full = intra_yr_stock_sent.set_index(['tickerName', 'date', 'minute', 'index'])

# # normalise values
# min max normilisation was used as it produced the required values of between 0 - 1 unlike mean normalisation
intra_sent_full_normilisation = min_max_normilisation(intra_sent_full)

# split dataset
train_intra_senti_70, test_intra_senti_30 = split_dataset(intra_sent_full_normilisation, 0.7, 2300)
train_intra_senti_75, test_intra_senti_25 = split_dataset(intra_sent_full_normilisation, 0.75, 2300)
train_intra_senti_80, test_intra_senti_20 = split_dataset(intra_sent_full_normilisation, 0.8, 2300)

xtest_intra_senti_30, ytest_intra_senti_30 = get_x_y(test_intra_senti_30, 'close')
xtest_intra_senti_25, ytest_intra_senti_25 = get_x_y(test_intra_senti_25, 'close')
xtest_intra_senti_20, ytest_intra_senti_20 = get_x_y(test_intra_senti_20, 'close')

# ------------------------
#      INTRA NO  SENTIMENT
# ------------------------
intra_sent_full = intra_yr_stock_sent.set_index(['tickerName', 'date', 'minute', 'index'])

intra_no_senti_normalised = min_max_normilisation(intra_sent_full)
intra_no_senti_normalised = intra_no_senti_normalised.drop(axis=1, labels=['title_pos_senti', 'title_avg_pos_sent',
                                                                           'title_neg_senti', 'title_avg_neg_sentiment',
                                                                           'title_senti_difference',
                                                                           'article_pos_senti',
                                                                           'article_avg_pos_sentiment',
                                                                           'article_neg_senti',
                                                                           'article_avg_neg_sentiment',
                                                                           'article_senti_difference'])
# split dataset
train_intra_no_senti_70, test_intra_no_senti_30 = split_dataset(intra_no_senti_normalised, 0.7, 2300)
train_intra_no_senti_75, test_intra_no_senti_25 = split_dataset(intra_no_senti_normalised, 0.75, 2300)
train_intra_no_senti_80, test_intra_no_senti_20 = split_dataset(intra_no_senti_normalised, 0.8, 2300)

xtest_intra_no_senti_30, ytest_intra_no_senti_30 = get_x_y(test_intra_no_senti_30, 'close')
xtest_intra_no_senti_25, ytest_intra_no_senti_25 = get_x_y(test_intra_no_senti_25, 'close')
xtest_intra_no_senti_20, ytest_intra_no_senti_20 = get_x_y(test_intra_no_senti_20, 'close')

list_dir = ['svm_models']
folder_path = "gen_data\\"

results_dictionary = {}

for dir in list_dir:
    dir_path = folder_path + dir
    for file in os.listdir(dir_path):

        svm = svm_load(dir_path + "\\" + file)
        # 5yr senti
        if "5yr_senti_70_30" in file:
            prediction = svm.predict(xtest_5yr_senti_30)
            rmse_value = rmse(ytest_5yr_senti_30, prediction)
            results_dictionary[file] = [rmse_value,
                                        svm.best_estimator_.get_params()['kernel'],
                                        svm.best_estimator_.get_params()['degree'],
                                        svm.best_estimator_.get_params()['gamma'],
                                        svm.best_estimator_.get_params()['C'],
                                        svm.best_estimator_.get_params()['tol'],
                                        svm.best_estimator_.get_params()['max_iter'],
                                        "svm"]

            print(f"Model:{file} \t || {rmse_value} ")
            print('Kernel:', svm.best_estimator_.get_params()['kernel'])
            print('Polynomial degree:', svm.best_estimator_.get_params()['degree'])
            print('Gamma:', svm.best_estimator_.get_params()['gamma'])
            print('C:', svm.best_estimator_.get_params()['C'])
            print('Tolerance:', svm.best_estimator_.get_params()['tol'])
            print('Max number of iterations', svm.best_estimator_.get_params()['max_iter'], "\n\n")
        elif "5yr_senti_75_25" in file:
            prediction = svm.predict(xtest_5yr_senti_25)
            rmse_value = rmse(ytest_5yr_senti_25, prediction)
            results_dictionary[file] = [rmse_value,
                                        svm.best_estimator_.get_params()['kernel'],
                                        svm.best_estimator_.get_params()['degree'],
                                        svm.best_estimator_.get_params()['gamma'],
                                        svm.best_estimator_.get_params()['C'],
                                        svm.best_estimator_.get_params()['tol'],
                                        svm.best_estimator_.get_params()['max_iter'],
                                        "svm"]

            print(f"Model:{file} \t || {rmse_value} ")
            print('Kernel:', svm.best_estimator_.get_params()['kernel'])
            print('Polynomial degree:', svm.best_estimator_.get_params()['degree'])
            print('Gamma:', svm.best_estimator_.get_params()['gamma'])
            print('C:', svm.best_estimator_.get_params()['C'])
            print('Tolerance:', svm.best_estimator_.get_params()['tol'])
            print('Max number of iterations', svm.best_estimator_.get_params()['max_iter'], "\n\n")
        elif "5yr_senti_80_20" in file:
            prediction = svm.predict(xtest_5yr_senti_20)
            rmse_value = rmse(ytest_5yr_senti_20, prediction)
            results_dictionary[file] = [rmse_value,
                                        svm.best_estimator_.get_params()['kernel'],
                                        svm.best_estimator_.get_params()['degree'],
                                        svm.best_estimator_.get_params()['gamma'],
                                        svm.best_estimator_.get_params()['C'],
                                        svm.best_estimator_.get_params()['tol'],
                                        svm.best_estimator_.get_params()['max_iter'],
                                        "svm"]

            print(f"Model:{file} \t || {rmse_value} ")
            print('Kernel:', svm.best_estimator_.get_params()['kernel'])
            print('Polynomial degree:', svm.best_estimator_.get_params()['degree'])
            print('Gamma:', svm.best_estimator_.get_params()['gamma'])
            print('C:', svm.best_estimator_.get_params()['C'])
            print('Tolerance:', svm.best_estimator_.get_params()['tol'])
            print('Max number of iterations', svm.best_estimator_.get_params()['max_iter'], "\n\n")

        # 5 yr no senti

        if "5yr_no_senti_70_30" in file:
            prediction = svm.predict(xtest_5yr_no_senti_30)
            rmse_value = rmse(ytest_5yr_no_senti_30, prediction)
            results_dictionary[file] = [rmse_value,
                                        svm.best_estimator_.get_params()['kernel'],
                                        svm.best_estimator_.get_params()['degree'],
                                        svm.best_estimator_.get_params()['gamma'],
                                        svm.best_estimator_.get_params()['C'],
                                        svm.best_estimator_.get_params()['tol'],
                                        svm.best_estimator_.get_params()['max_iter'],
                                        "svm"]

            print(f"Model:{file} \t || {rmse_value} ")
            print('Kernel:', svm.best_estimator_.get_params()['kernel'])
            print('Polynomial degree:', svm.best_estimator_.get_params()['degree'])
            print('Gamma:', svm.best_estimator_.get_params()['gamma'])
            print('C:', svm.best_estimator_.get_params()['C'])
            print('Tolerance:', svm.best_estimator_.get_params()['tol'])
            print('Max number of iterations', svm.best_estimator_.get_params()['max_iter'], "\n\n")
        elif "5yr_no_senti_75_25" in file:
            prediction = svm.predict(xtest_5yr_no_senti_25)
            rmse_value = rmse(ytest_5yr_no_senti_25, prediction)
            results_dictionary[file] = [rmse_value,
                                        svm.best_estimator_.get_params()['kernel'],
                                        svm.best_estimator_.get_params()['degree'],
                                        svm.best_estimator_.get_params()['gamma'],
                                        svm.best_estimator_.get_params()['C'],
                                        svm.best_estimator_.get_params()['tol'],
                                        svm.best_estimator_.get_params()['max_iter'],
                                        "svm"]

            print(f"Model:{file} \t || {rmse_value} ")
            print('Kernel:', svm.best_estimator_.get_params()['kernel'])
            print('Polynomial degree:', svm.best_estimator_.get_params()['degree'])
            print('Gamma:', svm.best_estimator_.get_params()['gamma'])
            print('C:', svm.best_estimator_.get_params()['C'])
            print('Tolerance:', svm.best_estimator_.get_params()['tol'])
            print('Max number of iterations', svm.best_estimator_.get_params()['max_iter'], "\n\n")
        elif "5yr_no_senti_80_20" in file:
            prediction = svm.predict(xtest_5yr_no_senti_20)
            rmse_value = rmse(ytest_5yr_no_senti_20, prediction)
            results_dictionary[file] = [rmse_value,
                                        svm.best_estimator_.get_params()['kernel'],
                                        svm.best_estimator_.get_params()['degree'],
                                        svm.best_estimator_.get_params()['gamma'],
                                        svm.best_estimator_.get_params()['C'],
                                        svm.best_estimator_.get_params()['tol'],
                                        svm.best_estimator_.get_params()['max_iter'],
                                        "svm"]

            print(f"Model:{file} \t || {rmse_value} ")
            print('Kernel:', svm.best_estimator_.get_params()['kernel'])
            print('Polynomial degree:', svm.best_estimator_.get_params()['degree'])
            print('Gamma:', svm.best_estimator_.get_params()['gamma'])
            print('C:', svm.best_estimator_.get_params()['C'])
            print('Tolerance:', svm.best_estimator_.get_params()['tol'])
            print('Max number of iterations', svm.best_estimator_.get_params()['max_iter'], "\n\n")

        # intra senti

        if "intra_senti_70_30" in file:
            prediction = svm.predict(xtest_intra_senti_30)
            rmse_value = rmse(ytest_intra_senti_30, prediction)
            results_dictionary[file] = [rmse_value,
                                        svm.best_estimator_.get_params()['kernel'],
                                        svm.best_estimator_.get_params()['degree'],
                                        svm.best_estimator_.get_params()['gamma'],
                                        svm.best_estimator_.get_params()['C'],
                                        svm.best_estimator_.get_params()['tol'],
                                        svm.best_estimator_.get_params()['max_iter'],
                                        "svm"]

            print(f"Model:{file} \t || {rmse_value} ")
            print('Kernel:', svm.best_estimator_.get_params()['kernel'])
            print('Polynomial degree:', svm.best_estimator_.get_params()['degree'])
            print('Gamma:', svm.best_estimator_.get_params()['gamma'])
            print('C:', svm.best_estimator_.get_params()['C'])
            print('Tolerance:', svm.best_estimator_.get_params()['tol'])
            print('Max number of iterations', svm.best_estimator_.get_params()['max_iter'], "\n\n")
        elif "intra_senti_75_25" in file:
            prediction = svm.predict(xtest_intra_senti_25)
            rmse_value = rmse(ytest_intra_senti_25, prediction)
            results_dictionary[file] = [rmse_value,
                                        svm.best_estimator_.get_params()['kernel'],
                                        svm.best_estimator_.get_params()['degree'],
                                        svm.best_estimator_.get_params()['gamma'],
                                        svm.best_estimator_.get_params()['C'],
                                        svm.best_estimator_.get_params()['tol'],
                                        svm.best_estimator_.get_params()['max_iter'],
                                        "svm"]

            print(f"Model:{file} \t || {rmse_value} ")
            print('Kernel:', svm.best_estimator_.get_params()['kernel'])
            print('Polynomial degree:', svm.best_estimator_.get_params()['degree'])
            print('Gamma:', svm.best_estimator_.get_params()['gamma'])
            print('C:', svm.best_estimator_.get_params()['C'])
            print('Tolerance:', svm.best_estimator_.get_params()['tol'])
            print('Max number of iterations', svm.best_estimator_.get_params()['max_iter'], "\n\n")
        elif "intra_senti_80_20" in file:
            prediction = svm.predict(xtest_intra_senti_20)
            rmse_value = rmse(ytest_intra_senti_20, prediction)
            results_dictionary[file] = [rmse_value,
                                        svm.best_estimator_.get_params()['kernel'],
                                        svm.best_estimator_.get_params()['degree'],
                                        svm.best_estimator_.get_params()['gamma'],
                                        svm.best_estimator_.get_params()['C'],
                                        svm.best_estimator_.get_params()['tol'],
                                        svm.best_estimator_.get_params()['max_iter'],
                                        "svm"]

            print(f"Model:{file} \t || {rmse_value} ")
            print('Kernel:', svm.best_estimator_.get_params()['kernel'])
            print('Polynomial degree:', svm.best_estimator_.get_params()['degree'])
            print('Gamma:', svm.best_estimator_.get_params()['gamma'])
            print('C:', svm.best_estimator_.get_params()['C'])
            print('Tolerance:', svm.best_estimator_.get_params()['tol'])
            print('Max number of iterations', svm.best_estimator_.get_params()['max_iter'], "\n\n")

        # intra no senti

        if "intra_no_senti_70_30" in file:
            prediction = svm.predict(xtest_intra_no_senti_30)
            rmse_value = rmse(ytest_intra_no_senti_30, prediction)
            results_dictionary[file] = [rmse_value,
                                        svm.best_estimator_.get_params()['kernel'],
                                        svm.best_estimator_.get_params()['degree'],
                                        svm.best_estimator_.get_params()['gamma'],
                                        svm.best_estimator_.get_params()['C'],
                                        svm.best_estimator_.get_params()['tol'],
                                        svm.best_estimator_.get_params()['max_iter'],
                                        "svm"]

            print(f"Model:{file} \t || {rmse_value} ")
            print('Kernel:', svm.best_estimator_.get_params()['kernel'])
            print('Polynomial degree:', svm.best_estimator_.get_params()['degree'])
            print('Gamma:', svm.best_estimator_.get_params()['gamma'])
            print('C:', svm.best_estimator_.get_params()['C'])
            print('Tolerance:', svm.best_estimator_.get_params()['tol'])
            print('Max number of iterations', svm.best_estimator_.get_params()['max_iter'], "\n\n")
        elif "intra_no_senti_75_25" in file:
            prediction = svm.predict(xtest_intra_no_senti_25)
            rmse_value = rmse(ytest_intra_senti_25, prediction)
            results_dictionary[file] = [rmse_value,
                                        svm.best_estimator_.get_params()['kernel'],
                                        svm.best_estimator_.get_params()['degree'],
                                        svm.best_estimator_.get_params()['gamma'],
                                        svm.best_estimator_.get_params()['C'],
                                        svm.best_estimator_.get_params()['tol'],
                                        svm.best_estimator_.get_params()['max_iter'],
                                        "svm"]

            print(f"Model:{file} \t || {rmse_value} ")
            print('Kernel:', svm.best_estimator_.get_params()['kernel'])
            print('Polynomial degree:', svm.best_estimator_.get_params()['degree'])
            print('Gamma:', svm.best_estimator_.get_params()['gamma'])
            print('C:', svm.best_estimator_.get_params()['C'])
            print('Tolerance:', svm.best_estimator_.get_params()['tol'])
            print('Max number of iterations', svm.best_estimator_.get_params()['max_iter'], "\n\n")
        elif "intra_no_senti_80_20" in file:
            prediction = svm.predict(xtest_intra_no_senti_20)
            rmse_value = rmse(ytest_intra_no_senti_20, prediction)
            results_dictionary[file] = [rmse_value,
                                        svm.best_estimator_.get_params()['kernel'],
                                        svm.best_estimator_.get_params()['degree'],
                                        svm.best_estimator_.get_params()['gamma'],
                                        svm.best_estimator_.get_params()['C'],
                                        svm.best_estimator_.get_params()['tol'],
                                        svm.best_estimator_.get_params()['max_iter'],
                                        "svm"]

            print(f"Model:{file} \t \n RMSE: {rmse_value} ")
            print('Kernel:', svm.best_estimator_.get_params()['kernel'])
            print('Polynomial degree:', svm.best_estimator_.get_params()['degree'])
            print('Gamma:', svm.best_estimator_.get_params()['gamma'])
            print('C:', svm.best_estimator_.get_params()['C'])
            print('Tolerance:', svm.best_estimator_.get_params()['tol'])
            print('Max number of iterations', svm.best_estimator_.get_params()['max_iter'], "\n\n")



pdf = pd.DataFrame.from_dict(data=results_dictionary,orient='index',columns=['rmse_value',
                                                                             'kernel',
                                                                             'degree',
                                                                             'gamma',
                                                                             'C',
                                                                             'tol',
                                                                             'max_iter',
                                                                             'model_type']
                             )

engine = create_engine("mysql+pymysql://{user}:{pw}@localhost/{db}"
                       .format(user="root",
                               pw="rootpassword",
                               db="thesisdb"))


pdf = pdf.reset_index()
pdf = pdf.rename(columns={'index':'model_name'})
print(pdf)
pdf.to_sql('svm_results', con=engine, if_exists='append')


# if dir == "cnn_models":
#     model_type = "cnn"
#     model = k_load_model(dir_path + "\\" + file)
#     prediction = 0.0
#     if "5yr_full_70_30" in file:
#         rmse = results_keras(xtest_5yr_senti_30_keras,ytest_5yr_senti_30_keras,file,model)
#         results_dictionary[file] = [rmse,model_type]
#     elif "5yr_full_75_25" in file:
#         prediction = model.predict(xtest_5yr_senti_25_keras)
#         rmse_value = rmse(ytest_5yr_senti_25_keras, prediction)
#         rmse = results_keras(xtest_5yr_senti_25_keras,ytest_5yr_senti_25_keras,file,model)
#         results_dictionary[file] = [rmse,model_type]
#     elif "5yr_full_80_20" in file:
#         rmse = results_keras(xtest_5yr_senti_20_keras,ytest_5yr_senti_20_keras,file,model)
#         results_dictionary[file] = [rmse,model_type]
#
#     elif "5yr_no_senti_70_30" in file:
#         rmse = results_keras(xtest_5yr_no_senti_30_keras,ytest_5yr_no_senti_30_keras,file,model)
#         results_dictionary[file] = [rmse,model_type]
#     elif "5yr_no_senti_75_25" in file:
#         rmse = results_keras(xtest_5yr_no_senti_25_keras,ytest_5yr_no_senti_25_keras,file,model)
#         results_dictionary[file] = [rmse,model_type]
#     elif "5yr_no_senti_80_20" in file:
#         rmse = results_keras(xtest_5yr_no_senti_20_keras,ytest_5yr_no_senti_20_keras,file,model)
#         results_dictionary[file] = [rmse,model_type]
#
#     elif "intra_senti_70_30" in file:
#         rmse = results_keras(xtest_intra_senti_30_keras,ytest_intra_senti_30_keras,file,model)
#         results_dictionary[file] = [rmse,model_type]
#     elif "intra_senti_75_25" in file:
#         rmse = results_keras(xtest_intra_senti_25_keras, ytest_intra_senti_25_keras, file, model)
#         results_dictionary[file] = [rmse, model_type]
#     elif "intra_senti_80_20" in file:
#         rmse = results_keras(xtest_intra_senti_20_keras,ytest_intra_senti_20_keras,file,model)
#         results_dictionary[file] = [rmse,model_type]
#
#     elif "intra_no_senti_70_30" in file:
#         rmse = results_keras(xtest_intra_no_senti_30_keras,ytest_intra_no_senti_30_keras,file,model)
#         results_dictionary[file] = [rmse,model_type]
#     elif "intra_no_senti_75_25" in file:
#         rmse = results_keras(xtest_intra_no_senti_25_keras,ytest_intra_no_senti_25_keras,file,model)
#         results_dictionary[file] = [rmse,model_type]
#     elif "intra_no_senti_80_20" in file:
#         rmse = results_keras(xtest_intra_no_senti_20_keras,ytest_intra_no_senti_20_keras,file,model)
#         results_dictionary[file] = [rmse,model_type]
