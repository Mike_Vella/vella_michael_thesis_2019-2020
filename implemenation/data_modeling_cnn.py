import pandas as pd
from keras.layers import Conv1D, MaxPooling1D, Flatten
from keras.models import Sequential
from keras.layers import Dense
from keras.models import save_model as k_save_model
import sklearn.metrics as metrics
from sqlalchemy import create_engine

def min_max_normilisation(dataframe):
    dataframe_normalised = (dataframe - dataframe.min()) / (dataframe.max() - dataframe.min())

    dataframe_normalised = dataframe_normalised.dropna()
    return dataframe_normalised


def split_dataset(dataset, test_size, seed):
    train = dataset.sample(frac=test_size, random_state=seed)
    # sampling removes the data present in the dataframe
    # Drops columns that contain the training data, and samples the full remainder of data.
    test = dataset.drop(train.index).sample(frac=1.0)
    return train, test


def get_x_y(dataframe, yColumn):
    y = dataframe[f'{yColumn}']
    x = dataframe.drop(axis=1, labels=f'{yColumn}')

    xnumpy = pd.DataFrame.to_numpy(x)
    ynumpy = pd.DataFrame.to_numpy(y)

    shape_of_x = xnumpy.shape
    xnumpy = xnumpy.reshape((shape_of_x[0], 1, shape_of_x[1]))

    return xnumpy, ynumpy


def rmse(ytest, pred):
    return metrics.mean_squared_error(ytest, pred, squared=False)


# define model
def create_model_basic(x_train, y_train, x_test, y_test, activation: str,
                       input_shape: tuple, dense_activation: str, compile_loss: str
                       , epcohs: int):
    model = Sequential()
    # model.add(Conv1D(filters=1, kernel_size=1, activation='relu', input_shape=(1,15)))
    model.add(Conv1D(filters=1, kernel_size=1, activation=activation, input_shape=input_shape))
    model.add(MaxPooling1D(pool_size=1))
    model.add(Flatten())
    model.add(Dense(100, activation=dense_activation))
    model.add(Dense(15))
    # the below dense was added
    model.add(Dense(1))
    model.compile(optimizer='adam', loss=[compile_loss])
    model.fit(x=x_train, y=y_train,
              validation_data=(x_test, y_test),
              epochs=epcohs, batch_size=100)
    return model


def save_model(dict_model: dict):
    for model_name, actual_model in dict_model.items():
        k_save_model(actual_model,f"gen_data\\cnn_models\\{model_name}")


def show_results(dict_model: dict,xtest, ytest):
    '''
    :param dict_model: dictionary of models
    :param ytest: only pass ytest if the previous models used the same ytest
    '''
    for model_name, actual_model in dict_model.items():
        model_score = actual_model.evaluate(xtest, ytest, batch_size=100)
        prediction = actual_model.predict(xtest)
        test_values = actual_model.test_on_batch(xtest, ytest)
        rmse_value = rmse(ytest, prediction)
        print(f"\nCurrent Model: {model_name}")
        print(f"Overall {model_name} loss using MSA: ", model_score)
        print(f"RMSE Value of {model_name}: ", rmse_value)
        print(f"MSA of tested values: ", test_values)
        dictionary_results[model_name] = [rmse_value,'cnn']


dictionary_results = {}
# importing data for both fiveyr and intra
five_yr_stock_sent = pd.read_csv("gen_data/fiveyr_stock_sentiment_full.csv")
intra_yr_stock_sent = pd.read_csv("gen_data/intra_stock_sentiment_full.csv")


# --------------------------------
#   5yr SENTIMENT PREPARATION
# --------------------------------

five_yr_sent_full_with_senti = five_yr_stock_sent.reset_index(drop=True)
five_yr_sent_full_with_senti = five_yr_sent_full_with_senti.set_index(['tickerName', 'date', 'index'])

five_yr_sent_full_normalised = min_max_normilisation(five_yr_sent_full_with_senti)

# split dataset
train_5yr_senti_70, test_5yr_senti_30 = split_dataset(five_yr_sent_full_normalised, 0.7, 2300)
train_5yr_senti_75, test_5yr_senti_25 = split_dataset(five_yr_sent_full_normalised, 0.75, 2300)
train_5yr_senti_80, test_5yr_senti_20 = split_dataset(five_yr_sent_full_normalised, 0.8, 2300)

# --------------------------------
# 5yr SENTIMENT 70/30 CNN Models
# --------------------------------
xtrain_5yr_senti_70, ytrain_5yr_senti_70 = get_x_y(train_5yr_senti_70, 'close')
xtest_5yr_senti_30, ytest_5yr_senti_30 = get_x_y(test_5yr_senti_30, 'close')

dict_model_70_30_senti = {}
model_cnn_sigmoid_relu_5yr_full_70_30 = create_model_basic(xtrain_5yr_senti_70, ytrain_5yr_senti_70, xtest_5yr_senti_30, ytest_5yr_senti_30, 'sigmoid', (1, 15), 'relu', 'mean_absolute_error', 20)
dict_model_70_30_senti['model_cnn_sigmoid_relu_5yr_full_70_30'] = model_cnn_sigmoid_relu_5yr_full_70_30

model_cnn_sigmoid_sigmoid_5yr_full_70_30 = create_model_basic(xtrain_5yr_senti_70, ytrain_5yr_senti_70, xtest_5yr_senti_30, ytest_5yr_senti_30, 'sigmoid', (1, 15), 'sigmoid', 'mean_absolute_error', 20)
dict_model_70_30_senti['model_cnn_sigmoid_sigmoid_5yr_full_70_30'] = model_cnn_sigmoid_sigmoid_5yr_full_70_30

model_cnn_relu_relu_5yr_full_70_30 = create_model_basic(xtrain_5yr_senti_70, ytrain_5yr_senti_70, xtest_5yr_senti_30, ytest_5yr_senti_30, 'relu', (1, 15), 'relu', 'mean_absolute_error', 20)
dict_model_70_30_senti['model_cnn_relu_relu_5yr_full_70_30'] = model_cnn_relu_relu_5yr_full_70_30

model_cnn_relu_sigmoid_5yr_full_70_30 = create_model_basic(xtrain_5yr_senti_70, ytrain_5yr_senti_70, xtest_5yr_senti_30, ytest_5yr_senti_30, 'relu', (1, 15), 'sigmoid', 'mean_absolute_error', 20)
dict_model_70_30_senti['model_cnn_relu_sigmoid_5yr_full_70_30'] = model_cnn_relu_sigmoid_5yr_full_70_30

save_model(dict_model_70_30_senti)
show_results(dict_model_70_30_senti, xtest_5yr_senti_30, ytest_5yr_senti_30)

# --------------------------------
# 5yr SENTIMENT 75/25 CNN Models
# --------------------------------
xtrain_5yr_senti_75, ytrain_5yr_senti_75 = get_x_y(train_5yr_senti_75, 'close')
xtest_5yr_senti_25,ytest_5yr_senti_25 = get_x_y(test_5yr_senti_25,'close')

dict_model_75_25_senti = {}

model_cnn_sigmoid_relu_5yr_full_75_25 = create_model_basic(xtrain_5yr_senti_75, ytrain_5yr_senti_75, xtest_5yr_senti_25, ytest_5yr_senti_25,'sigmoid', (1, 15), 'relu', 'mean_absolute_error', 20)
dict_model_75_25_senti['model_cnn_sigmoid_relu_5yr_full_75_25'] = model_cnn_sigmoid_relu_5yr_full_75_25

model_cnn_sigmoid_sigmoid_5yr_full_75_25 = create_model_basic(xtrain_5yr_senti_75, ytrain_5yr_senti_75, xtest_5yr_senti_25, ytest_5yr_senti_25, 'sigmoid', (1, 15), 'sigmoid', 'mean_absolute_error', 20)
dict_model_75_25_senti['model_cnn_sigmoid_sigmoid_5yr_full_75_25'] = model_cnn_sigmoid_sigmoid_5yr_full_75_25

model_cnn_relu_relu_5yr_full_75_25 = create_model_basic(xtrain_5yr_senti_75, ytrain_5yr_senti_75, xtest_5yr_senti_25, ytest_5yr_senti_25,'relu', (1, 15), 'relu', 'mean_absolute_error', 20)
dict_model_75_25_senti['model_cnn_relu_relu_5yr_full_75_25'] = model_cnn_relu_relu_5yr_full_75_25

model_cnn_relu_sigmoid_5yr_full_75_25 = create_model_basic(xtrain_5yr_senti_75, ytrain_5yr_senti_75, xtest_5yr_senti_25, ytest_5yr_senti_25, 'relu', (1, 15), 'sigmoid', 'mean_absolute_error', 20)
dict_model_75_25_senti['model_cnn_relu_sigmoid_5yr_full_75_25'] = model_cnn_relu_sigmoid_5yr_full_75_25

show_results(dict_model_75_25_senti, xtest_5yr_senti_25, ytest_5yr_senti_25)
save_model(dict_model_75_25_senti)

# --------------------------------
# 5yr SENTIMENT 80/20 CNN Models
# --------------------------------
xtrain_5yr_senti_80, ytrain_5yr_senti_80 = get_x_y(train_5yr_senti_80,'close')
xtest_5yr_senti_20, ytest_5yr_senti_20 = get_x_y(test_5yr_senti_20,'close')

dict_model_80_20_senti = {}

model_cnn_sigmoid_relu_5yr_full_80_20 = create_model_basic(xtrain_5yr_senti_80, ytrain_5yr_senti_80, xtest_5yr_senti_20, ytest_5yr_senti_20,'sigmoid', (1, 15), 'relu', 'mean_absolute_error', 20)
dict_model_80_20_senti['model_cnn_sigmoid_relu_5yr_full_80_20'] = model_cnn_sigmoid_relu_5yr_full_80_20

model_cnn_sigmoid_sigmoid_5yr_full_80_20 = create_model_basic(xtrain_5yr_senti_80, ytrain_5yr_senti_80, xtest_5yr_senti_20, ytest_5yr_senti_20, 'sigmoid', (1, 15), 'sigmoid', 'mean_absolute_error', 20)
dict_model_80_20_senti['model_cnn_sigmoid_sigmoid_5yr_full_80_20'] = model_cnn_sigmoid_sigmoid_5yr_full_80_20

model_cnn_relu_relu_5yr_full_80_20 = create_model_basic(xtrain_5yr_senti_80, ytrain_5yr_senti_80, xtest_5yr_senti_20, ytest_5yr_senti_20,'relu', (1, 15), 'relu', 'mean_absolute_error', 20)
dict_model_80_20_senti['model_cnn_relu_relu_5yr_full_80_20'] = model_cnn_relu_relu_5yr_full_80_20

model_cnn_relu_sigmoid_5yr_full_80_20 = create_model_basic(xtrain_5yr_senti_80, ytrain_5yr_senti_80, xtest_5yr_senti_20, ytest_5yr_senti_20, 'relu', (1, 15), 'sigmoid', 'mean_absolute_error', 20)
dict_model_80_20_senti['model_cnn_relu_sigmoid_5yr_full_80_20'] = model_cnn_relu_sigmoid_5yr_full_80_20

show_results(dict_model_80_20_senti,xtest_5yr_senti_20, ytest_5yr_senti_20)
save_model(dict_model_80_20_senti)

# --------------------------------
#   5yr NO SENTIMENT PREPARATION
# --------------------------------
five_yr_no_senti_full = five_yr_stock_sent.set_index(['tickerName', 'date', 'index'])

five_yr_no_senti_normalised = min_max_normilisation(five_yr_no_senti_full)
five_yr_no_senti_normalised = five_yr_no_senti_normalised.drop(axis=1,
                                                               labels=['title_pos_sentiment',
                                                                       'title_avg_pos_sent',
                                                                       'title_neg_sentiment',
                                                                       'title_avg_neg_sentiment',
                                                                       'title_sentiment_difference',
                                                                       'article_pos_sentiment',
                                                                       'article_avg_pos_sentiment',
                                                                       'article_neg_sentiment',
                                                                       'article_avg_neg_sentiment',
                                                                       'article_sentiment_difference'])
# split dataset
train_no_senti_70, test_no_senti_30 = split_dataset(five_yr_no_senti_normalised, 0.7, 2300)
train_no_senti_75, test_no_senti_25 = split_dataset(five_yr_no_senti_normalised, 0.75, 2300)
train_no_senti_80, test_no_senti_20 = split_dataset(five_yr_no_senti_normalised, 0.8, 2300)

# --------------------------------
# 5yr NO SENTIMENT 70/30 CNN Models
# --------------------------------
xtrain_5yr_no_senti_70, ytrain_5yr_no_senti_70 = get_x_y(train_no_senti_70, 'close')
xtest_5yr_no_senti_30, ytest_5yr_no_senti_30 = get_x_y(test_no_senti_30, 'close')
dict_model_70_30_no_senti = {}

model_cnn_sigmoid_relu_5yr_no_senti_70_30 = create_model_basic(xtrain_5yr_no_senti_70, ytrain_5yr_no_senti_70, xtest_5yr_no_senti_30, ytest_5yr_no_senti_30, 'sigmoid', (1, 5), 'relu', 'mean_absolute_error', 1)
dict_model_70_30_no_senti['model_cnn_sigmoid_relu_5yr_no_senti_70_30'] = model_cnn_sigmoid_relu_5yr_no_senti_70_30

model_cnn_sigmoid_sigmoid_5yr_no_senti_70_30 = create_model_basic(xtrain_5yr_no_senti_70, ytrain_5yr_no_senti_70, xtest_5yr_no_senti_30, ytest_5yr_no_senti_30, 'sigmoid', (1, 5), 'sigmoid', 'mean_absolute_error', 20)
dict_model_70_30_no_senti['model_cnn_sigmoid_sigmoid_5yr_no_senti_70_30'] = model_cnn_sigmoid_sigmoid_5yr_no_senti_70_30

model_cnn_relu_relu_5yr_no_senti_70_30 = create_model_basic(xtrain_5yr_no_senti_70, ytrain_5yr_no_senti_70, xtest_5yr_no_senti_30, ytest_5yr_no_senti_30, 'relu', (1, 5), 'relu', 'mean_absolute_error', 20)
dict_model_70_30_no_senti['model_cnn_relu_relu_5yr_no_senti_70_30'] = model_cnn_relu_relu_5yr_no_senti_70_30

model_cnn_relu_sigmoid_5yr_no_senti_70_30 = create_model_basic(xtrain_5yr_no_senti_70, ytrain_5yr_no_senti_70, xtest_5yr_no_senti_30, ytest_5yr_no_senti_30, 'relu', (1, 5), 'sigmoid', 'mean_absolute_error', 20)
dict_model_70_30_no_senti['model_cnn_relu_sigmoid_5yr_no_senti_70_30'] = model_cnn_relu_sigmoid_5yr_no_senti_70_30

show_results(dict_model_70_30_no_senti, xtest_5yr_no_senti_30, ytest_5yr_no_senti_30)
save_model(dict_model_70_30_no_senti)

# --------------------------------
# 5yr NO SENTIMENT 75/25 CNN Models
# --------------------------------
xtrain_5yr_no_senti_75, ytrain_5yr_no_senti_75 = get_x_y(train_no_senti_75, 'close')
xtest_5yr_no_senti_25, ytest_5yr_no_senti_25 = get_x_y(test_no_senti_25, 'close')
dict_model_75_25_no_senti = {}

model_cnn_sigmoid_relu_5yr_no_senti_75_25 = create_model_basic(xtrain_5yr_no_senti_75, ytrain_5yr_no_senti_75, xtest_5yr_no_senti_25, ytest_5yr_no_senti_25, 'sigmoid', (1, 5), 'relu', 'mean_absolute_error', 1)
dict_model_75_25_no_senti['model_cnn_sigmoid_relu_5yr_no_senti_75_25'] = model_cnn_sigmoid_relu_5yr_no_senti_75_25

model_cnn_sigmoid_sigmoid_5yr_no_senti_75_25 = create_model_basic(xtrain_5yr_no_senti_75, ytrain_5yr_no_senti_75, xtest_5yr_no_senti_25, ytest_5yr_no_senti_25, 'sigmoid', (1, 5), 'sigmoid', 'mean_absolute_error', 20)
dict_model_75_25_no_senti['model_cnn_sigmoid_sigmoid_5yr_no_senti_75_25'] = model_cnn_sigmoid_sigmoid_5yr_no_senti_75_25

model_cnn_relu_relu_5yr_no_senti_70_30 = create_model_basic(xtrain_5yr_no_senti_75, ytrain_5yr_no_senti_75, xtest_5yr_no_senti_25, ytest_5yr_no_senti_25, 'relu', (1, 5), 'relu', 'mean_absolute_error', 20)
dict_model_75_25_no_senti['model_cnn_relu_relu_5yr_no_senti_70_30'] = model_cnn_relu_relu_5yr_no_senti_70_30

model_cnn_relu_sigmoid_5yr_no_senti_70_30 = create_model_basic(xtrain_5yr_no_senti_75, ytrain_5yr_no_senti_75, xtest_5yr_no_senti_25, ytest_5yr_no_senti_25, 'relu', (1, 5), 'sigmoid', 'mean_absolute_error', 20)
dict_model_75_25_no_senti['model_cnn_relu_sigmoid_5yr_no_senti_70_30'] = model_cnn_relu_sigmoid_5yr_no_senti_70_30

show_results(dict_model_75_25_no_senti,xtest_5yr_no_senti_25, ytest_5yr_no_senti_25)
save_model(dict_model_75_25_no_senti)

# --------------------------------
# 5yr NO SENTIMENT 80/20 CNN Models
# --------------------------------
xtrain_5yr_no_senti_80, ytrain_5yr_no_senti_80 = get_x_y(train_no_senti_80, 'close')
xtest_5yr_no_senti_20, ytest_5yr_no_senti_20 = get_x_y(test_no_senti_20, 'close')
dict_model_80_20_no_senti = {}

model_cnn_sigmoid_relu_5yr_no_senti_80_20 = create_model_basic(xtrain_5yr_no_senti_80, ytrain_5yr_no_senti_80, xtest_5yr_no_senti_20, ytest_5yr_no_senti_20, 'sigmoid', (1, 5), 'relu', 'mean_absolute_error', 1)
dict_model_80_20_no_senti['model_cnn_sigmoid_relu_5yr_no_senti_80_20'] = model_cnn_sigmoid_relu_5yr_no_senti_80_20

model_cnn_sigmoid_sigmoid_5yr_no_senti_75_25 = create_model_basic(xtrain_5yr_no_senti_80, ytrain_5yr_no_senti_80, xtest_5yr_no_senti_20, ytest_5yr_no_senti_20, 'sigmoid', (1, 5), 'sigmoid', 'mean_absolute_error', 20)
dict_model_80_20_no_senti['model_cnn_sigmoid_sigmoid_5yr_no_senti_75_25'] = model_cnn_sigmoid_sigmoid_5yr_no_senti_75_25

model_cnn_relu_relu_5yr_no_senti_80_20 = create_model_basic(xtrain_5yr_no_senti_80, ytrain_5yr_no_senti_80, xtest_5yr_no_senti_20, ytest_5yr_no_senti_20, 'relu', (1, 5), 'relu', 'mean_absolute_error', 20)
dict_model_80_20_no_senti['model_cnn_relu_relu_5yr_no_senti_80_20'] = model_cnn_relu_relu_5yr_no_senti_80_20

model_cnn_relu_sigmoid_5yr_no_senti_80_20 = create_model_basic(xtrain_5yr_no_senti_80, ytrain_5yr_no_senti_80, xtest_5yr_no_senti_20, ytest_5yr_no_senti_20, 'relu', (1, 5), 'sigmoid', 'mean_absolute_error', 20)
dict_model_80_20_no_senti['model_cnn_relu_sigmoid_5yr_no_senti_80_20'] = model_cnn_relu_sigmoid_5yr_no_senti_80_20

show_results(dict_model_80_20_no_senti,xtest_5yr_no_senti_20, ytest_5yr_no_senti_20)
save_model(dict_model_80_20_no_senti)

# ------------------------
#      INTRA SENTIMENT
# ------------------------

intra_stock_sent = intra_yr_stock_sent.reset_index(drop=True)
intra_sent_full = intra_yr_stock_sent.set_index(['tickerName', 'date', 'minute', 'index'])

# # normalise values
# min max normilisation was used as it produced the required values of between 0 - 1 unlike mean normalisation
intra_sent_full_normilisation = min_max_normilisation(intra_sent_full)

# split dataset
train_intra_senti_70, test_intra_senti_30 = split_dataset(intra_sent_full_normilisation, 0.7, 2300)
train_intra_senti_75, test_intra_senti_25 = split_dataset(intra_sent_full_normilisation, 0.75, 2300)
train_intra_senti_80, test_intra_senti_20 = split_dataset(intra_sent_full_normilisation, 0.8, 2300)

# --------------------------------
# Intra SENTIMENT 70/30 CNN Models
# --------------------------------
xtrain_intra_senti_70, ytrain_intra_senti_70 = get_x_y(train_intra_senti_70, 'close')
xtest_intra_senti_30, ytest_intra_senti_30 = get_x_y(test_intra_senti_30, 'close')
dict_intra_model_70_30_senti ={}

model_cnn_sigmoid_relu_intra_senti_70_30 = create_model_basic(xtrain_intra_senti_70, ytrain_intra_senti_70, xtest_intra_senti_30, ytest_intra_senti_30, 'sigmoid', (1, 15), 'relu', 'mean_absolute_error', 1)
dict_intra_model_70_30_senti['model_cnn_sigmoid_relu_intra_senti_70_30'] = model_cnn_sigmoid_relu_intra_senti_70_30

model_cnn_sigmoid_sigmoid_intra_senti_70_30 = create_model_basic(xtrain_intra_senti_70, ytrain_intra_senti_70, xtest_intra_senti_30, ytest_intra_senti_30, 'sigmoid', (1, 15), 'sigmoid', 'mean_absolute_error', 20)
dict_intra_model_70_30_senti['model_cnn_sigmoid_sigmoid_intra_senti_70_30'] = model_cnn_sigmoid_sigmoid_intra_senti_70_30

model_cnn_relu_relu_intra_senti_70_30 = create_model_basic(xtrain_intra_senti_70, ytrain_intra_senti_70, xtest_intra_senti_30, ytest_intra_senti_30, 'relu', (1, 15), 'relu', 'mean_absolute_error', 20)
dict_intra_model_70_30_senti['model_cnn_relu_relu_intra_senti_70_30'] = model_cnn_relu_relu_intra_senti_70_30

model_cnn_relu_sigmoid_intra_senti_70_30 = create_model_basic(xtrain_intra_senti_70, ytrain_intra_senti_70, xtest_intra_senti_30, ytest_intra_senti_30, 'relu', (1, 15), 'sigmoid', 'mean_absolute_error', 20)
dict_intra_model_70_30_senti['model_cnn_relu_sigmoid_intra_senti_70_30'] = model_cnn_relu_sigmoid_intra_senti_70_30

show_results(dict_intra_model_70_30_senti, xtest_intra_senti_30, ytest_intra_senti_30)
save_model(dict_intra_model_70_30_senti)

# --------------------------------
# Intra SENTIMENT 75/25 CNN Models
# --------------------------------
xtrain_intra_senti_75, ytrain_intra_senti_75 = get_x_y(train_intra_senti_75, 'close')
xtest_intra_senti_25, ytest_intra_senti_25 = get_x_y(test_intra_senti_25, 'close')
dict_intra_model_75_25_senti ={}

model_cnn_sigmoid_relu_intra_senti_75_25 = create_model_basic(xtrain_intra_senti_75, ytrain_intra_senti_75, xtest_intra_senti_25, ytest_intra_senti_25, 'sigmoid', (1, 15), 'relu', 'mean_absolute_error', 1)
dict_intra_model_75_25_senti['model_cnn_sigmoid_relu_intra_senti_75_25'] = model_cnn_sigmoid_relu_intra_senti_75_25

model_cnn_sigmoid_sigmoid_intra_senti_75_25 = create_model_basic(xtrain_intra_senti_75, ytrain_intra_senti_75, xtest_intra_senti_25, ytest_intra_senti_25, 'sigmoid', (1, 15), 'sigmoid', 'mean_absolute_error', 20)
dict_intra_model_75_25_senti['model_cnn_sigmoid_sigmoid_intra_senti_75_25'] = model_cnn_sigmoid_sigmoid_intra_senti_75_25

model_cnn_relu_relu_intra_senti_75_25 = create_model_basic(xtrain_intra_senti_75, ytrain_intra_senti_75, xtest_intra_senti_25, ytest_intra_senti_25, 'relu', (1, 15), 'relu', 'mean_absolute_error', 20)
dict_intra_model_75_25_senti['model_cnn_relu_relu_intra_senti_75_25'] = model_cnn_relu_relu_intra_senti_75_25

model_cnn_relu_sigmoid_intra_senti_75_25 = create_model_basic(xtrain_intra_senti_75, ytrain_intra_senti_75, xtest_intra_senti_25, ytest_intra_senti_25, 'relu', (1, 15), 'sigmoid', 'mean_absolute_error', 20)
dict_intra_model_75_25_senti['model_cnn_relu_sigmoid_intra_senti_75_25'] = model_cnn_relu_sigmoid_intra_senti_75_25

show_results(dict_intra_model_75_25_senti,xtest_intra_senti_25, ytest_intra_senti_25)
save_model(dict_intra_model_75_25_senti)

# --------------------------------
# Intra SENTIMENT 80/20 CNN Models
# --------------------------------
xtrain_intra_senti_80, ytrain_intra_senti_80 = get_x_y(train_intra_senti_80, 'close')
xtest_intra_senti_20, ytest_intra_senti_20 = get_x_y(test_intra_senti_20, 'close')
dict_intra_model_80_20_senti ={}

model_cnn_sigmoid_relu_intra_senti_80_20 = create_model_basic(xtrain_intra_senti_80, ytrain_intra_senti_80, xtest_intra_senti_20, ytest_intra_senti_20, 'sigmoid', (1, 15), 'relu', 'mean_absolute_error', 1)
dict_intra_model_80_20_senti['model_cnn_sigmoid_relu_intra_senti_80_20'] = model_cnn_sigmoid_relu_intra_senti_80_20

model_cnn_sigmoid_sigmoid_intra_senti_80_20 = create_model_basic(xtrain_intra_senti_80, ytrain_intra_senti_80, xtest_intra_senti_20, ytest_intra_senti_20, 'sigmoid', (1, 15), 'sigmoid', 'mean_absolute_error', 20)
dict_intra_model_80_20_senti['model_cnn_sigmoid_sigmoid_intra_senti_80_20'] = model_cnn_sigmoid_sigmoid_intra_senti_80_20

model_cnn_relu_relu_intra_senti_80_20 = create_model_basic(xtrain_intra_senti_80, ytrain_intra_senti_80, xtest_intra_senti_20, ytest_intra_senti_20, 'relu', (1, 15), 'relu', 'mean_absolute_error', 20)
dict_intra_model_80_20_senti['model_cnn_relu_relu_intra_senti_80_20'] = model_cnn_relu_relu_intra_senti_80_20

model_cnn_relu_sigmoid_intra_senti_80_20 = create_model_basic(xtrain_intra_senti_80, ytrain_intra_senti_80, xtest_intra_senti_20, ytest_intra_senti_20, 'relu', (1, 15), 'sigmoid', 'mean_absolute_error', 20)
dict_intra_model_80_20_senti['model_cnn_relu_sigmoid_intra_senti_80_20'] = model_cnn_relu_sigmoid_intra_senti_80_20

show_results(dict_intra_model_80_20_senti,xtest_intra_senti_20, ytest_intra_senti_20)
save_model(dict_intra_model_80_20_senti)

# ----------------------------
#       INTRA NO SENTIMENT
# ----------------------------
intra_sent_full = intra_yr_stock_sent.set_index(['tickerName', 'date', 'minute', 'index'])

intra_no_senti_normalised = min_max_normilisation(intra_sent_full)
intra_no_senti_normalised = intra_no_senti_normalised.drop(axis=1,
                                                               labels=['title_pos_senti',
                                                                       'title_avg_pos_sent',
                                                                       'title_neg_senti',
                                                                       'title_avg_neg_sentiment',
                                                                       'title_senti_difference',
                                                                       'article_pos_senti',
                                                                       'article_avg_pos_sentiment',
                                                                       'article_neg_senti',
                                                                       'article_avg_neg_sentiment',
                                                                       'article_senti_difference'])
# split dataset
train_intra_no_senti_70, test_intra_no_senti_30 = split_dataset(intra_no_senti_normalised, 0.7, 2300)
train_intra_no_senti_75, test_intra_no_senti_25 = split_dataset(intra_no_senti_normalised, 0.75, 2300)
train_intra_no_senti_80, test_intra_no_senti_20 = split_dataset(intra_no_senti_normalised, 0.8, 2300)

# -----------------------------------------
#   Intra NO SENTIMENT 70/30 CNN Models
# -----------------------------------------
xtrain_intra_no_senti_70, ytrain_intra_no_senti_70 = get_x_y(train_intra_no_senti_70, 'close')
xtest_intra_no_senti_30, ytest_intra_no_senti_30 = get_x_y(test_intra_no_senti_30, 'close')
dict_intra_model_70_30_no_senti = {}

model_cnn_sigmoid_relu_no_intra_senti_70_30 = create_model_basic(xtrain_intra_no_senti_70, ytrain_intra_no_senti_70, xtest_intra_no_senti_30, ytest_intra_no_senti_30, 'sigmoid', (1, 5), 'relu', 'mean_absolute_error', 1)
dict_intra_model_70_30_no_senti['model_cnn_sigmoid_relu_no_intra_senti_70_30'] = model_cnn_sigmoid_relu_no_intra_senti_70_30

model_cnn_sigmoid_sigmoid_no_intra_senti_70_30  = create_model_basic(xtrain_intra_no_senti_70, ytrain_intra_no_senti_70, xtest_intra_no_senti_30, ytest_intra_no_senti_30, 'sigmoid', (1, 5), 'sigmoid', 'mean_absolute_error', 20)
dict_intra_model_70_30_no_senti['model_cnn_sigmoid_sigmoid_no_intra_senti_70_30'] = model_cnn_sigmoid_sigmoid_no_intra_senti_70_30

model_cnn_relu_relu_no_intra_senti_70_30  = create_model_basic(xtrain_intra_no_senti_70, ytrain_intra_no_senti_70, xtest_intra_no_senti_30, ytest_intra_no_senti_30, 'relu', (1, 5), 'relu', 'mean_absolute_error', 20)
dict_intra_model_70_30_no_senti['model_cnn_relu_relu_no_intra_senti_70_30'] = model_cnn_relu_relu_no_intra_senti_70_30

model_cnn_relu_sigmoid_no_intra_senti_70_30  = create_model_basic(xtrain_intra_no_senti_70, ytrain_intra_no_senti_70, xtest_intra_no_senti_30, ytest_intra_no_senti_30, 'relu', (1, 5), 'sigmoid', 'mean_absolute_error', 20)
dict_intra_model_70_30_no_senti['model_cnn_relu_sigmoid_no_intra_senti_70_30'] = model_cnn_relu_sigmoid_no_intra_senti_70_30

show_results(dict_intra_model_70_30_no_senti,xtest_intra_no_senti_30, ytest_intra_no_senti_30)
save_model(dict_intra_model_70_30_no_senti)

# -----------------------------------------
#   Intra NO SENTIMENT 75/25 CNN Models
# -----------------------------------------
xtrain_intra_no_senti_75, ytrain_intra_no_senti_75 = get_x_y(train_intra_no_senti_75, 'close')
xtest_intra_no_senti_25, ytest_intra_no_senti_25 = get_x_y(test_intra_no_senti_25, 'close')
dict_intra_model_75_25_no_senti = {}

model_cnn_sigmoid_relu_intra_no_senti_75_25 = create_model_basic(xtrain_intra_no_senti_75, ytrain_intra_no_senti_75, xtest_intra_no_senti_25, ytest_intra_no_senti_25, 'sigmoid', (1, 5), 'relu', 'mean_absolute_error', 1)
dict_intra_model_75_25_no_senti['model_cnn_sigmoid_relu_intra_no_senti_75_25'] = model_cnn_sigmoid_relu_intra_no_senti_75_25

model_cnn_sigmoid_sigmoid_intra_no_senti_75_25 = create_model_basic(xtrain_intra_no_senti_75, ytrain_intra_no_senti_75, xtest_intra_no_senti_25, ytest_intra_no_senti_25, 'sigmoid', (1, 5), 'sigmoid', 'mean_absolute_error', 20)
dict_intra_model_75_25_no_senti['model_cnn_sigmoid_sigmoid_intra_no_senti_75_25'] = model_cnn_sigmoid_sigmoid_intra_no_senti_75_25

model_cnn_relu_relu_intra_no_senti_75_25 = create_model_basic(xtrain_intra_no_senti_75, ytrain_intra_no_senti_75, xtest_intra_no_senti_25, ytest_intra_no_senti_25, 'relu', (1, 5), 'relu', 'mean_absolute_error', 20)
dict_intra_model_75_25_no_senti['model_cnn_relu_relu_intra_no_senti_75_25'] = model_cnn_relu_relu_intra_no_senti_75_25

model_cnn_relu_sigmoid_intra_no_senti_75_25 = create_model_basic(xtrain_intra_no_senti_75, ytrain_intra_no_senti_75, xtest_intra_no_senti_25, ytest_intra_no_senti_25, 'relu', (1, 5), 'sigmoid', 'mean_absolute_error', 20)
dict_intra_model_75_25_no_senti['model_cnn_relu_sigmoid_intra_no_senti_75_25'] = model_cnn_relu_sigmoid_intra_no_senti_75_25

show_results(dict_intra_model_75_25_no_senti, xtest_intra_no_senti_25, ytest_intra_no_senti_25)
save_model(dict_intra_model_75_25_no_senti)

# -----------------------------------------
#   Intra NO SENTIMENT 80/20 CNN Models
# -----------------------------------------
xtrain_intra_no_senti_80, ytrain_intra_no_senti_80 = get_x_y(train_intra_no_senti_80, 'close')
xtest_intra_no_senti_20, ytest_intra_no_senti_20 = get_x_y(test_intra_no_senti_20, 'close')
dict_intra_model_80_20_no_senti = {}

model_cnn_sigmoid_relu_intra_no_senti_80_20 = create_model_basic(xtrain_intra_no_senti_80, ytrain_intra_no_senti_80, xtest_intra_no_senti_20, ytest_intra_no_senti_20, 'sigmoid', (1, 5), 'relu', 'mean_absolute_error', 1)
dict_intra_model_80_20_no_senti['model_cnn_sigmoid_relu_intra_no_senti_80_20'] = model_cnn_sigmoid_relu_intra_no_senti_80_20

model_cnn_sigmoid_sigmoid_intra_no_senti_80_20 = create_model_basic(xtrain_intra_no_senti_80, ytrain_intra_no_senti_80, xtest_intra_no_senti_20, ytest_intra_no_senti_20, 'sigmoid', (1, 5), 'sigmoid', 'mean_absolute_error', 20)
dict_intra_model_80_20_no_senti['model_cnn_sigmoid_sigmoid_intra_no_senti_80_20'] = model_cnn_sigmoid_sigmoid_intra_no_senti_80_20

model_cnn_relu_relu_intra_no_senti_80_20 = create_model_basic(xtrain_intra_no_senti_80, ytrain_intra_no_senti_80, xtest_intra_no_senti_20, ytest_intra_no_senti_20, 'relu', (1, 5), 'relu', 'mean_absolute_error', 20)
dict_intra_model_80_20_no_senti['model_cnn_relu_relu_intra_no_senti_80_20'] = model_cnn_relu_relu_intra_no_senti_80_20

model_cnn_relu_sigmoid_intra_no_senti_80_20 = create_model_basic(xtrain_intra_no_senti_80, ytrain_intra_no_senti_80, xtest_intra_no_senti_20, ytest_intra_no_senti_20, 'relu', (1, 5), 'sigmoid', 'mean_absolute_error', 20)
dict_intra_model_80_20_no_senti['model_cnn_relu_sigmoid_intra_no_senti_80_20'] = model_cnn_relu_sigmoid_intra_no_senti_80_20

show_results(dict_intra_model_80_20_no_senti,xtest_intra_no_senti_20, ytest_intra_no_senti_20)
save_model(dict_intra_model_80_20_no_senti)



engine = create_engine("mysql+pymysql://{user}:{pw}@localhost/{db}"
                       .format(user="root",
                               pw="rootpassword",
                               db="thesisdb"))

pdf = pd.DataFrame.from_dict(data=dictionary_results,orient='index',columns=['model_rmse','model_type'])
pdf = pdf.reset_index()
pdf = pdf.rename(columns={'index':'model_name'})
print(pdf)
pdf.to_sql('keras_results', con=engine, if_exists='append')