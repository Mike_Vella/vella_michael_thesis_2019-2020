import pandas as pd
import sklearn.metrics as metrics
from keras.layers import Dense, LSTM
from keras.models import Sequential
from keras.models import save_model as k_save_model
from sqlalchemy import create_engine

def min_max_normilisation(dataframe):
    dataframe_normalised = (dataframe - dataframe.min()) / (dataframe.max() - dataframe.min())

    dataframe_normalised = dataframe_normalised.dropna()
    return dataframe_normalised


def split_dataset(dataset, test_size, seed):
    train = dataset.sample(frac=test_size, random_state=seed)
    # sampling removes the data present in the dataframe
    # Drops columns that contain the training data, and samples the full remainder of data.
    test = dataset.drop(train.index).sample(frac=1.0)
    return train, test


def get_x_y(dataframe, yColumn):
    y = dataframe[f'{yColumn}']
    x = dataframe.drop(axis=1, labels=f'{yColumn}')

    xnumpy = pd.DataFrame.to_numpy(x)
    ynumpy = pd.DataFrame.to_numpy(y)

    shape_of_x = xnumpy.shape
    xnumpy = xnumpy.reshape((shape_of_x[0], 1, shape_of_x[1]))

    return xnumpy, ynumpy


def rmse(ytest, pred):
    return metrics.mean_squared_error(ytest, pred, squared=False)


def create_model(x_train, y_train, x_test, y_test,
                 input_shape: tuple, compile_loss: str
                 , epcohs: int):
    rnn = Sequential()
    rnn.add(LSTM(units=60, return_sequences=True, input_shape=input_shape))
    # regressor.add(Dropout(0.1))
    rnn.add(LSTM(units=50, return_sequences=True))
    # regressor.add(Dropout(0.1))
    rnn.add(LSTM(units=40, return_sequences=True))
    # regressor.add(Dropout(0.1))
    rnn.add(LSTM(units=30))
    # regressor.add(Dropout(0.2))
    rnn.add(Dense(units=1))
    rnn.compile(optimizer='adam', loss=compile_loss)
    rnn.fit(x_train, y_train, validation_data=(x_test, y_test),
            epochs=epcohs, batch_size=100)
    return rnn


def save_model(dict_model: dict):
    for model_name, actual_model in dict_model.items():
        k_save_model(actual_model,f'gen_data\\rnn_models\\{model_name}')


def show_results(model_name, actual_model, xtest, ytest):
    '''
    :param dict_model: dictionary of models
    :param ytest: only pass ytest if the previous models used the same ytest
    '''
    model_score = actual_model.evaluate(xtest, ytest, batch_size=100)
    prediction = actual_model.predict(xtest)
    test_values = actual_model.test_on_batch(xtest, ytest)
    rmse_value = rmse(ytest, prediction)
    print(f"\nCurrent Model: {model_name}")
    print(f"Overall {model_name} loss using MSA: ", model_score)
    print(f"RMSE Value of {model_name}: ", rmse(ytest, prediction))
    print(f"MSA of tested values: ", test_values)
    dictionary_results[model_name] = [rmse_value, 'rnn']


dictionary_results = {}

five_yr_stock_sent = pd.read_csv("gen_data/fiveyr_stock_sentiment_full.csv")
intra_yr_stock_sent = pd.read_csv("gen_data/intra_stock_sentiment_full.csv")

# --------------------------------
#   5yr SENTIMENT PREPARATION
# --------------------------------

five_yr_sent_full_with_senti = five_yr_stock_sent.reset_index(drop=True)
five_yr_sent_full_with_senti = five_yr_sent_full_with_senti.set_index(['tickerName', 'date', 'index'])

five_yr_sent_full_normalised = min_max_normilisation(five_yr_sent_full_with_senti)

# split dataset
train_5yr_senti_70, test_5yr_senti_30 = split_dataset(five_yr_sent_full_normalised, 0.7, 2300)
train_5yr_senti_75, test_5yr_senti_25 = split_dataset(five_yr_sent_full_normalised, 0.75, 2300)
train_5yr_senti_80, test_5yr_senti_20 = split_dataset(five_yr_sent_full_normalised, 0.8, 2300)

# --------------------------------
# 5yr SENTIMENT RNN LSTM Models
# --------------------------------
xtrain_5yr_senti_70, ytrain_5yr_senti_70 = get_x_y(train_5yr_senti_70, 'close')
xtest_5yr_senti_30, ytest_5yr_senti_30 = get_x_y(test_5yr_senti_30, 'close')

xtrain_5yr_senti_75, ytrain_5yr_senti_75 = get_x_y(train_5yr_senti_75, 'close')
xtest_5yr_senti_25, ytest_5yr_senti_25 = get_x_y(test_5yr_senti_25, 'close')

xtrain_5yr_senti_80, ytrain_5yr_senti_80 = get_x_y(train_5yr_senti_80, 'close')
xtest_5yr_senti_20, ytest_5yr_senti_20 = get_x_y(test_5yr_senti_20, 'close')

dict_model_rnn_5yr_senti_full = {}
model_rnn_5yr_full_70_30 = create_model(xtrain_5yr_senti_70, ytrain_5yr_senti_70, xtest_5yr_senti_30, ytest_5yr_senti_30, (1, 15), 'mean_absolute_error', 16)
dict_model_rnn_5yr_senti_full['model_rnn_5yr_full_70_30'] = model_rnn_5yr_full_70_30

model_rnn_5yr_full_75_25 = create_model(xtrain_5yr_senti_75, ytrain_5yr_senti_75, xtest_5yr_senti_25, ytest_5yr_senti_25, (1, 15), 'mean_absolute_error', 16)
dict_model_rnn_5yr_senti_full['model_rnn_5yr_full_75_25'] = model_rnn_5yr_full_75_25

model_rnn_5yr_full_80_20 = create_model(xtrain_5yr_senti_80, ytrain_5yr_senti_80, xtest_5yr_senti_20, ytest_5yr_senti_20, (1, 15), 'mean_absolute_error', 16)
dict_model_rnn_5yr_senti_full['model_rnn_5yr_full_80_20'] = model_rnn_5yr_full_80_20

save_model(dict_model_rnn_5yr_senti_full)
show_results("model_rnn_5yr_full_70_30", model_rnn_5yr_full_70_30, xtest_5yr_senti_30, ytest_5yr_senti_30)
show_results("model_rnn_5yr_full_75_25", model_rnn_5yr_full_75_25, xtest_5yr_senti_25, ytest_5yr_senti_25)
show_results("model_rnn_5yr_full_80_20", model_rnn_5yr_full_80_20, xtest_5yr_senti_20, ytest_5yr_senti_20)

# --------------------------------
#   5yr NO SENTIMENT PREPARATION
# --------------------------------
five_yr_no_senti_full = five_yr_stock_sent.set_index(['tickerName', 'date', 'index'])

five_yr_no_senti_normalised = min_max_normilisation(five_yr_no_senti_full)
five_yr_no_senti_normalised = five_yr_no_senti_normalised.drop(axis=1,
                                                               labels=['title_pos_sentiment',
                                                                       'title_avg_pos_sent',
                                                                       'title_neg_sentiment',
                                                                       'title_avg_neg_sentiment',
                                                                       'title_sentiment_difference',
                                                                       'article_pos_sentiment',
                                                                       'article_avg_pos_sentiment',
                                                                       'article_neg_sentiment',
                                                                       'article_avg_neg_sentiment',
                                                                       'article_sentiment_difference'])
# split dataset
train_no_senti_70, test_no_senti_30 = split_dataset(five_yr_no_senti_normalised, 0.7, 2300)
train_no_senti_75, test_no_senti_25 = split_dataset(five_yr_no_senti_normalised, 0.75, 2300)
train_no_senti_80, test_no_senti_20 = split_dataset(five_yr_no_senti_normalised, 0.8, 2300)

# --------------------------------
# 5yr NO SENTIMENT RNN Models
# --------------------------------
xtrain_5yr_no_senti_70, ytrain_5yr_no_senti_70 = get_x_y(train_no_senti_70, 'close')
xtest_5yr_no_senti_30, ytest_5yr_no_senti_30 = get_x_y(test_no_senti_30, 'close')

xtrain_5yr_no_senti_75, ytrain_5yr_no_senti_75 = get_x_y(train_no_senti_75, 'close')
xtest_5yr_no_senti_25, ytest_5yr_no_senti_25 = get_x_y(test_no_senti_25, 'close')

xtrain_5yr_no_senti_80, ytrain_5yr_no_senti_80 = get_x_y(train_no_senti_80, 'close')
xtest_5yr_no_senti_20, ytest_5yr_no_senti_20 = get_x_y(test_no_senti_20, 'close')

dict_model_rnn_5yr_no_senti = {}

model_rnn_5yr_no_senti_70_30 = create_model(xtrain_5yr_no_senti_70, ytrain_5yr_no_senti_70, xtest_5yr_no_senti_30, ytest_5yr_no_senti_30, (1, 5), 'mean_absolute_error', 16)
dict_model_rnn_5yr_no_senti['model_rnn_5yr_no_senti_70_30'] = model_rnn_5yr_no_senti_70_30

model_rnn_5yr_no_senti_75_25 = create_model(xtrain_5yr_no_senti_75, ytrain_5yr_no_senti_75, xtest_5yr_no_senti_25, ytest_5yr_no_senti_25, (1, 5), 'mean_absolute_error', 16)
dict_model_rnn_5yr_no_senti['model_rnn_5yr_no_senti_75_25'] = model_rnn_5yr_no_senti_75_25

model_rnn_5yr_no_senti_80_20 = create_model(xtrain_5yr_no_senti_80, ytrain_5yr_no_senti_80, xtest_5yr_no_senti_20, ytest_5yr_no_senti_20, (1, 5), 'mean_absolute_error', 16)
dict_model_rnn_5yr_no_senti['model_rnn_5yr_no_senti_80_20'] = model_rnn_5yr_no_senti_80_20

show_results("model_rnn_5yr_no_senti_70_30", model_rnn_5yr_no_senti_70_30, xtest_5yr_no_senti_30, ytest_5yr_no_senti_30)
show_results("model_rnn_5yr_no_senti_75_25", model_rnn_5yr_no_senti_75_25, xtest_5yr_no_senti_25, ytest_5yr_no_senti_25)
show_results("model_rnn_5yr_no_senti_80_20", model_rnn_5yr_no_senti_80_20, xtest_5yr_no_senti_20, ytest_5yr_no_senti_20)
save_model(dict_model_rnn_5yr_no_senti)

# ------------------------
#      INTRA SENTIMENT
# ------------------------

intra_stock_sent = intra_yr_stock_sent.reset_index(drop=True)
intra_sent_full = intra_yr_stock_sent.set_index(['tickerName', 'date', 'minute', 'index'])

# # normalise values
# min max normilisation was used as it produced the required values of between 0 - 1 unlike mean normalisation
intra_sent_full_normilisation = min_max_normilisation(intra_sent_full)

# split dataset
train_intra_senti_70, test_intra_senti_30 = split_dataset(intra_sent_full_normilisation, 0.7, 2300)
train_intra_senti_75, test_intra_senti_25 = split_dataset(intra_sent_full_normilisation, 0.75, 2300)
train_intra_senti_80, test_intra_senti_20 = split_dataset(intra_sent_full_normilisation, 0.8, 2300)

# --------------------------------
# Intra SENTIMENT RNN Models
# --------------------------------
xtrain_intra_senti_70, ytrain_intra_senti_70 = get_x_y(train_intra_senti_70, 'close')
xtest_intra_senti_30, ytest_intra_senti_30 = get_x_y(test_intra_senti_30, 'close')

xtrain_intra_senti_75, ytrain_intra_senti_75 = get_x_y(train_intra_senti_75, 'close')
xtest_intra_senti_25, ytest_intra_senti_25 = get_x_y(test_intra_senti_25, 'close')

xtrain_intra_senti_80, ytrain_intra_senti_80 = get_x_y(train_intra_senti_80, 'close')
xtest_intra_senti_20, ytest_intra_senti_20 = get_x_y(test_intra_senti_20, 'close')

dict_intra_rnn_senti = {}

model_rnn_intra_senti_70_30 = create_model(xtrain_intra_senti_70, ytrain_intra_senti_70, xtest_intra_senti_30, ytest_intra_senti_30, (1, 15), 'mean_absolute_error', 16)
dict_intra_rnn_senti['model_rnn_intra_senti_70_30'] = model_rnn_intra_senti_70_30

model_rnn_intra_senti_75_25 = create_model(xtrain_intra_senti_75, ytrain_intra_senti_75, xtest_intra_senti_25, ytest_intra_senti_25, (1, 15), 'mean_absolute_error', 16)
dict_intra_rnn_senti['model_rnn_intra_senti_75_25'] = model_rnn_intra_senti_75_25

model_rnn_intra_senti_80_20 = create_model(xtrain_intra_senti_80, ytrain_intra_senti_80, xtest_intra_senti_20, ytest_intra_senti_20, (1, 15), 'mean_absolute_error', 16)
dict_intra_rnn_senti['model_rnn_intra_senti_80_20'] = model_rnn_intra_senti_80_20

show_results("model_rnn_intra_senti_70_30", model_rnn_intra_senti_70_30, xtest_intra_senti_30, ytest_intra_senti_30)
show_results("model_rnn_intra_senti_75_25", model_rnn_intra_senti_75_25, xtest_intra_senti_25, ytest_intra_senti_25)
show_results("model_rnn_intra_senti_80_20", model_rnn_intra_senti_75_25, xtest_intra_senti_20, ytest_intra_senti_20)
save_model(dict_intra_rnn_senti)

# ----------------------------
#       INTRA NO SENTIMENT
# ----------------------------
intra_sent_full = intra_yr_stock_sent.set_index(['tickerName', 'date', 'minute', 'index'])

intra_no_senti_normalised = min_max_normilisation(intra_sent_full)
intra_no_senti_normalised = intra_no_senti_normalised.drop(axis=1,
                                                           labels=['title_pos_senti',
                                                                   'title_avg_pos_sent',
                                                                   'title_neg_senti',
                                                                   'title_avg_neg_sentiment',
                                                                   'title_senti_difference',
                                                                   'article_pos_senti',
                                                                   'article_avg_pos_sentiment',
                                                                   'article_neg_senti',
                                                                   'article_avg_neg_sentiment',
                                                                   'article_senti_difference'])
# split dataset
train_intra_no_senti_70, test_intra_no_senti_30 = split_dataset(intra_no_senti_normalised, 0.7, 2300)
train_intra_no_senti_75, test_intra_no_senti_25 = split_dataset(intra_no_senti_normalised, 0.75, 2300)
train_intra_no_senti_80, test_intra_no_senti_20 = split_dataset(intra_no_senti_normalised, 0.8, 2300)

# -----------------------------------------
#   Intra NO SENTIMENT RNN Models
# -----------------------------------------
xtrain_intra_no_senti_70, ytrain_intra_no_senti_70 = get_x_y(train_intra_no_senti_70, 'close')
xtest_intra_no_senti_30, ytest_intra_no_senti_30 = get_x_y(test_intra_no_senti_30, 'close')

xtrain_intra_no_senti_75, ytrain_intra_no_senti_75 = get_x_y(train_intra_no_senti_75, 'close')
xtest_intra_no_senti_25, ytest_intra_no_senti_25 = get_x_y(test_intra_no_senti_25, 'close')

xtrain_intra_no_senti_80, ytrain_intra_no_senti_80 = get_x_y(train_intra_no_senti_80, 'close')
xtest_intra_no_senti_20, ytest_intra_no_senti_20 = get_x_y(test_intra_no_senti_20, 'close')

dict_intra_model_rnn_no_senti = {}

model_rnn_no_intra_senti_70_30 = create_model(xtrain_intra_no_senti_70, ytrain_intra_no_senti_70, xtest_intra_no_senti_30, ytest_intra_no_senti_30, (1, 5), 'mean_absolute_error', 16)
dict_intra_model_rnn_no_senti['model_rnn_no_intra_senti_70_30'] = model_rnn_no_intra_senti_70_30

model_rnn_no_intra_senti_75_25 = create_model(xtrain_intra_no_senti_75, ytrain_intra_no_senti_75, xtest_intra_no_senti_25, ytest_intra_no_senti_25, (1, 5), 'mean_absolute_error', 16)
dict_intra_model_rnn_no_senti['model_rnn_no_intra_senti_75_25'] = model_rnn_no_intra_senti_75_25

model_rnn_no_intra_senti_80_20 = create_model(xtrain_intra_no_senti_80, ytrain_intra_no_senti_80, xtest_intra_no_senti_20, ytest_intra_no_senti_20, (1, 5), 'mean_absolute_error', 16)
dict_intra_model_rnn_no_senti['model_rnn_no_intra_senti_80_20'] = model_rnn_no_intra_senti_80_20


show_results("model_rnn_no_intra_senti_70_30",model_rnn_no_intra_senti_70_30, xtest_intra_no_senti_30, ytest_intra_no_senti_30)
show_results("model_rnn_no_intra_senti_75_25",model_rnn_no_intra_senti_75_25, xtest_intra_no_senti_25, ytest_intra_no_senti_25)
show_results("model_rnn_no_intra_senti_80_20",model_rnn_no_intra_senti_80_20, xtest_intra_no_senti_20, ytest_intra_no_senti_20)
save_model(dict_intra_model_rnn_no_senti)


engine = create_engine("mysql+pymysql://{user}:{pw}@localhost/{db}"
                       .format(user="root",
                               pw="rootpassword",
                               db="thesisdb"))

pdf = pd.DataFrame.from_dict(data=dictionary_results,orient='index',columns=['model_rmse','model_type'])
pdf = pdf.reset_index()
pdf = pdf.rename(columns={'index':'model_name'})
print(pdf)
pdf.to_sql('keras_results', con=engine, if_exists='append')