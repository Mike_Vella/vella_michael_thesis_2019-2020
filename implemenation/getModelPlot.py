import keras as keras
from keras.models import load_model

rnn_model = load_model("gen_data\\rnn_models\\model_rnn_5yr_full_70_30.h5")

keras.utils.plot_model(
    rnn_model,
    to_file="gen_data\\model_plots\\rnn_model.png",
    show_shapes=True,
    show_layer_names=True,
    rankdir="TB",
    expand_nested=False
)


cnn_model = load_model("gen_data\\cnn_models\\model_cnn_relu_relu_5yr_full_70_30.h5")

keras.utils.plot_model(
    cnn_model,
    to_file="gen_data\\model_plots\\cnn_model.png",
    show_shapes=True,
    show_layer_names=True,
    rankdir="TB",
    expand_nested=False
)
