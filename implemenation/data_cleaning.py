import pandas as pd
import datetime
import re

from sqlalchemy import create_engine

pd.set_option('display.max_columns', None)
pd.set_option('display.expand_frame_repr', False)
pd.set_option('max_colwidth', None)

news_articles = pd.read_json("gen_data/news_articles_time_stamp_clean.json")
print(news_articles)

# remove dupes
no_dupes = news_articles.set_index('id')
no_dupes = no_dupes.drop_duplicates(keep='first')
print(no_dupes)

no_dupes['timestamp'] = pd.to_datetime(no_dupes['timestamp'])

engine = create_engine(
    "mysql+pymysql://{user}:{pw}@localhost/{db}".format(user="root", pw="rootpassword", db="thesisdb"))

no_dupes.to_csv("C:\\vella_michael_thesis_2019-2020\implemenation\gen_data\\news_articles_no_dupes.csv", index=True,
                header=True, mode="w", sep=',')
no_dupes.to_sql('news_articles_no_dupes', con=engine, if_exists='append')
