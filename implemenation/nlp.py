import nltk
import sqlalchemy
import pandas as pd
import matplotlib.pyplot as plt
from nltk.corpus import stopwords
from nltk.stem import PorterStemmer
from nltk.stem.wordnet import WordNetLemmatizer
from nltk.corpus import sentiwordnet as swn
from nltk.sentiment import sentiment_analyzer
from sklearn.feature_extraction.text import CountVectorizer
from sklearn.feature_extraction.text import TfidfTransformer
from sklearn.feature_extraction.text import TfidfVectorizer
from collections import defaultdict

# import nltk.text as tfidf

# config for pandas
pd.set_option('display.max_columns', None)
pd.set_option('display.expand_frame_repr', False)
pd.set_option('max_colwidth', None)

# download punkt, stopwords, wordnet
nltk.download('punkt')
nltk.download('stopwords')
nltk.download('wordnet')
nltk.download('averaged_perceptron_tagger')
nltk.download('sentiwordnet')

# sql engine
engine = sqlalchemy.create_engine(
    "mysql+pymysql://{user}:{pw}@localhost/{db}".format(user="root", pw="rootpassword", db="thesisdb"))

sql_query = "select * from thesisdb.news_articles_five_yr"

# query db and store results
og_dataset = pd.read_sql_query(sql_query, engine).set_index('id')

titles = og_dataset['title'].to_dict()
articles = og_dataset['article'].to_dict()


def tokenise(value):
    token = nltk.tokenize.regexp_tokenize(text=value, pattern=r"\w+")
    return token


def remove_stop_words(tokenized_list=[], stop_word_dict=str):
    eng_stopwords = set(stopwords.words(stop_word_dict))
    filter_title = []

    for word in tokenized_list:
        if word not in eng_stopwords:
            filter_title.append(word)
    return filter_title


def lemmatise_word(list_to_lemma=list):
    lem = WordNetLemmatizer()
    lemmaList = []
    for w in list_to_lemma:
        lemmaList.append(lem.lemmatize(w.lower()))
    return lemmaList


def return_to_sentence(lemmatised_words):
    sentence = ""
    for w in lemmatised_words:
        sentence = sentence + " " + w
    return sentence


def preprocess(text_dict={}, process=str):
    print(f"Starting {process}")
    counter = 1
    for key, value in text_dict.items():
        # tokenize value
        token = tokenise(value)
        stop_word_removal = remove_stop_words(token, 'english')
        lemmatised_words = lemmatise_word(stop_word_removal)
        sentence = return_to_sentence(lemmatised_words)
        text_dict[key] = sentence
        print(f"Currently @ {process} # {counter} of {len(text_dict)}")
        counter += 1


def preform_tfidf(preprocessed_dict={}):
    preprocessed_text_list = list(preprocessed_dict.values())
    tfidf_vectorizer = TfidfVectorizer(ngram_range=(1, 2), use_idf=True, smooth_idf=True)
    fit = tfidf_vectorizer.fit_transform(preprocessed_text_list)
    feat_name = tfidf_vectorizer.get_feature_names()
    dense_matrix = fit.todense()
    list_dense = dense_matrix.tolist()
    df = pd.DataFrame(list_dense, columns=feat_name)
    return fit, feat_name, list_dense


def preform_classification_text(text_list, process):
    pos_dict = {}
    neg_dict = {}
    total_sentiment_score = {}
    print(f"Starting sentiment for {process}")
    counter = 1
    for id, title in text_list.items():
        token_title = tokenise(title)

        # Added tags to words so as they are identified
        tags = nltk.pos_tag(token_title)

        for word, tag in tags.__iter__():

            simple_tag = ''
            # noun
            if tag.startswith('NN'):
                simple_tag = 'n'
            # adjective
            elif tag.startswith('JJ'):
                simple_tag = 'a'
            # verb
            elif tag.startswith('V'):
                simple_tag = 'v'
            # adverb
            elif tag.startswith('R'):
                simple_tag = 'r'
            # cant be classified
            else:
                simple_tag = ''

            if simple_tag != '':
                senti_net = list(swn.senti_synsets(word, simple_tag))
                # word_score = 0
                # word sentiment
                word_positive = 0.0
                word_negative = 0.0
                if len(senti_net) > 0:
                    for syn in senti_net:
                        word_positive = syn.pos_score() + word_positive
                        word_negative = syn.neg_score() + word_negative
                        # word_score += syn.pos_score() - syn.neg_score()
                    word_positive = word_positive + word_positive
                    word_negative = word_negative + word_negative

                   # sentiment_dict[word] = (word_score / len(senti_net))
                    pos_dict[word] = word_positive
                    neg_dict[word] = word_negative

                    word_positive = 0.0
                    word_negative = 0.0


        # get avg
        total_pos_score = 0.0
        total_neg_score = 0.0
        for word, pos_score in pos_dict.items():
            total_pos_score = pos_score + total_pos_score

        for word, neg_score in neg_dict.items():
            total_neg_score = neg_score + total_neg_score

        total_sentiment_score[id] = total_pos_score, total_neg_score

        print(f"Currently @ {process} sentiment # {counter} of {len(text_list.items())}")
        counter += 1
    return list(total_sentiment_score.items())


preprocess(titles, 'titles')
preprocess(articles, 'articles')

# title_fit, title_feat_name, title_list_dense = preform_tfidf(titles)
# article_fit, article_feat_name, article_list_dense = preform_tfidf(articles)


title_sentiment = preform_classification_text(titles, 'titles')
article_sentiment = preform_classification_text(articles, 'articles')

title_sentiment_score_dict = {}
for id, title_sentiment_scores in title_sentiment:
    title_listed_scores = [title_sentiment_scores]
    for title_pos_score, title_neg_score in title_listed_scores:
        title_sentiment_score_dict[id] = title_pos_score, title_neg_score
        print(f"title_sentiment current values: {id}, {title_pos_score}, {title_neg_score}")

article_sentiment_score_dict = {}
for id, article_sentiment_score in article_sentiment:
    article_listed_scores = [article_sentiment_score]
    for article_pos_score, article_neg_score in article_listed_scores:
        article_sentiment_score_dict[id] = article_pos_score, article_neg_score
        print(f"article_sentiment current values: {id}, {article_pos_score}, {article_neg_score}")

combined_sentiment = defaultdict(list)

for dictionary in (title_sentiment_score_dict, article_sentiment_score_dict):
    for key, value in dictionary.items():
        combined_sentiment[key].append(value)

combined_sentiment = dict(combined_sentiment)
data = {}
for key, values in combined_sentiment.items():
    sent_list = [[a, b] for a, b in values]
    sent_list = sum(sent_list, [])
    data[key] = sent_list

sentiment_df = pd.DataFrame.from_dict(data=data, orient='index',
                                      columns=['title_pos_score', 'title_neg_score', 'article_pos_score','article_neg_score'])
sentiment_df.reset_index(level=0)
sentiment_df.to_sql('news_articles_sentiment', con=engine, if_exists='append')

# tfidf_vectorizer = TfidfVectorizer(use_idf=True)
# fitted_vectorizer = tfidf_vectorizer.fit(list)
# tfidf_vectorizer_vectors = fitted_vectorizer.transform(list)
#
# tfidf_vectorizer = TfidfVectorizer(ngram_range=(2, 2), use_idf=True, smooth_idf=True, stop_words='english')
# fit = tfidf_vectorizer.fit_transform(list)
# feat_name = tfidf_vectorizer.get_feature_names()
# # Return a dense matrix representation of this matrix.
# dense_matrix = fit.todense()
# list_dense = dense_matrix.tolist()
# df = pd.DataFrame(list_dense, columns=feat_name)
#
# # Now getting actual values instead of just 1.0 for each word.
# print("\n\nDataframe:\n", df)


# # Tokenise words
# tokened_title = nltk.tokenize.regexp_tokenize(text=frst_title, pattern=r"\w+")
# print(tokened_title)
#
# freqDist = nltk.probability.FreqDist(tokened_title)
#
# print(freqDist)
#
# # remove stop words
# eng_stopwords = set(stopwords.words('english'))
#
# filter_title = []
# for word in tokened_title:
#     if word not in eng_stopwords:
#         filter_title.append(word)
#
# print(filter_title)
#
# # Stemm and Lemma words
# # stemmer = PorterStemmer()
# # stemm_word = []
# # for w in filter_title:
# #     stemm_word.append(stemmer.stem(word=w.lower()))
#
# lem = WordNetLemmatizer()
# lemmaList = []
# for w in filter_title:
#     lemmaList.append(lem.lemmatize(w.lower()))
# ngram = list(nltk.ngrams(lemmaList, 2))
# print(lemmaList)
#
# clean_data = []
# # for word in l
#
# tfidf_vectorizer = TfidfVectorizer(use_idf=True)
# fitted_vectorizer = tfidf_vectorizer.fit(list)
# tfidf_vectorizer_vectors = fitted_vectorizer.transform(list)
#
# tfidf_vectorizer = TfidfVectorizer(ngram_range=(2, 2), use_idf=True, smooth_idf=True, stop_words='english')
# fit = tfidf_vectorizer.fit_transform(list)
# feat_name = tfidf_vectorizer.get_feature_names()
# # Return a dense matrix representation of this matrix.
# dense_matrix = fit.todense()
# list_dense = dense_matrix.tolist()
# df = pd.DataFrame(list_dense, columns=feat_name)
#
# # Now getting actual values instead of just 1.0 for each word.
# print("\n\nDataframe:\n", df)


# for title_id, title_sent in title_sentiment:
#     for article_id, article_sent in article_sentiment:
#         if title_id == article_id:
#             values[title_id] = title_sent, article_sent
#             print(f"Dictionary current values: {title_id}, {title_sent}, {article_sent}")

# combined_sentiment = {**title_sentiment_score_dict, **article_sentiment_score_dict}