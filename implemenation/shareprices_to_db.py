import mysql.connector as con
import pyEX as ex
import pandas as pd
import datetime as date_time
from datetime import timedelta
from sqlalchemy import create_engine

pd.set_option('display.max_columns', None)
pd.set_option('display.expand_frame_repr', False)
pd.set_option('max_colwidth', None)

# OLD CODE
# cursor.execute("create table thesisdb.stockprices ("
#                "stockprice_id int auto_increment primary key,"
#                "ticker varchar(30) not null,"
#                "stock_open float not null,"
#                "stock_high float not null,"
#                "stock_low float not null,"
#                "stock_close float not null,"
#                "stock_volume float not null,"
#                "stock_date datetime not null"
#                ");")

# date = date_time.datetime(2019, 12, 4, 00, 00, 00, 00)
# data = ex.chartDF('aapl', '1d', date, 'pk_854d0408386b48cdbd86d9330a3c7f2f', 'v1')
# pd.set_option('display.max_rows', 500)
# pd.set_option('display.max_columns', 500)
# pd.set_option('display.width', 1000)
# data = pd.DataFrame(data)

# class Db(object):
#
#     def __init__(self):
#         """Connects to the db and sets up cursor"""
#         self.db = con.connect(
#             host="localhost",
#             user="root",
#             passwd="rootpassword",
#             database="thesisdb"
#         )
#         self.cursor = self.db.cursor()

# print(data)

# db = Db()
# db.cursor.execute();


engine = create_engine("mysql+pymysql://{user}:{pw}@localhost/{db}"
                       .format(user="root",
                               pw="rootpassword",
                               db="thesisdb"))

api_key = 'pk_854d0408386b48cdbd86d9330a3c7f2f'


def insert_intraday_db(ticker_list: [], start_date: date_time.datetime, end_date: date_time.datetime):
    """
    Inserts ticker list into database starting from the start_date till the end_date

    :param ticker_list: string list
    :param start_date: datetime
    :param end_date: datetime
    :return:Nothing
    """

    for ticker in ticker_list:
        print(f"Inserting ticker: {ticker} for date range {start_date} - {end_date}")

        current_date = start_date
        delta = timedelta(days=1)
        diff = 0
        weekend = set([5, 6])
        current_date_str = current_date.strftime("%Y%m%d")
        while current_date <= end_date:
            if current_date.weekday() not in weekend:
                diff += 1
                current_date_str = current_date.strftime("%Y%m%d")
                print(F"Inserting data for {ticker} on date : {current_date_str}")

                data = ex.chartDF(ticker, '1d', current_date_str, api_key, 'v1')
                if data.empty:
                    print(F"No data available for date: {current_date_str}")
                else:
                    data['tickerName'] = ticker
                    data['tickerDate'] = current_date_str
                    # data['tickerNameDate'] = data['tickerName'].astype(str) + '_' + data['tickerDate'] + '_' + data['label'].astype(str)

                    info = pd.DataFrame(data=data)
                    # info.set_index('tickerNameDate')

                    info.to_csv(
                        "D:/thesis_2019-2020/gen_data/intraday_dataset/" + ticker + "_" + current_date_str + ".csv",
                        index=True, header=True, mode="w", sep=',')
                    info.to_sql('intraday_ticker_info', con=engine, if_exists='append')
            current_date += delta
        print(F"Insertion for ticker :{ticker} is completed\n")



def insert_5yr_year_db(ticker_list: [], end_date: date_time.datetime):
    """
    Inserts ticker list into database starting end_date till 5 years ago

    :param ticker_list: string list
    :param end_date: datetime
    :return:Nothing
    """

    for ticker in ticker_list:
        print(f"Inserting ticker: {ticker} from beginning to {end_date}")

        current_date = end_date
        current_date_str = current_date.strftime("%Y%m%d")
        print(F"Inserting data for {ticker} on date : {current_date_str}")

        data = ex.chartDF(ticker, '5y', current_date_str, api_key, 'v1')
        if data.empty:
            print(F"No data available for date: {current_date_str}")
        else:
            data['tickerName'] = ticker
            data['tickerDate'] = current_date_str
            print(data['label'])
            data['tickerNameDateTime'] = data['tickerName'].astype(str)

            info = pd.DataFrame(data=data)

            info.to_csv(
                "C:\\vella_michael_thesis_2019-2020\implemenation\gen_data\\5yr_dataset\\" + ticker + "_"".csv",
                index=True, header=True, mode="w", sep=',')
            info.to_sql('five_year_ticker_info', con=engine, if_exists='append')

    print(F"Insertion for ticker :{ticker} is completed\n")


tckr_lst = ['aapl', 'xom', 'pep', 'nvda', 'viaca','viac', 'jcp', 'wu', 'atvi', 'hog']


start_day = date_time.date(2019, 11, 17)
end_day = date_time.date(2020, 2, 17)

insert_intraday_db(tckr_lst, start_day, end_day)
insert_5yr_year_db(tckr_lst, end_day)