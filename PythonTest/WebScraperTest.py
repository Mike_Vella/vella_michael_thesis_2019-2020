# Test for webscapper.
from requests import get
from requests.exceptions import RequestException
from contextlib import closing
from bs4 import BeautifulSoup
import os
from datetime import datetime

import re


def is_good_response(resp):
    """
    :param resp:
    :return: True if response seems to be HTML, false otherwise
    """
    content_type = resp.headers['Content-Type'].lower()
    return (resp.status_code == 200
            and content_type is not True
            and content_type.find('html') > -1)


def log_error(e):
    """
    :param e: string version of reported error
    :return: prints string error
    """
    print(e)


def simple_get(url):
    """Attempts to get the content at 'url' by making an HTTP Get request.
       If the content type is pf some kind of HTML/XML, return the text content
       else return NONE
       """
    try:
        with closing(get(url, stream=True)) as resp:
            if is_good_response(resp):
                return resp.content
            else:
                return None

    except RequestException as e:
        log_error('Error during request to {0} : {1}'.format(url, str(e)))


doc_path = "D:/thesis_2019-2020/PythonTest/yahooFb.html"

html_document = simple_get("https://www.reuters.com//article/idUSKCN1VM1LK")

html = BeautifulSoup(html_document, 'html.parser')
articles = html.find(attrs={'class':'StandardArticleBody_container'})
for article in articles:
    article.get('p')
    print(article.get_text())


# writes to file
# if os.path.isfile(doc_path):
#     os.remove(doc_path)
#     with open(doc_path, "a") as f:
#         f.write("<h1>Created on {0}</h1></br>".format(datetime.now()))
#     for i in articles:
#         with open(doc_path, "a") as f:
#             f.write(str(i))
# else:
#     with open(doc_path, "a") as f:
#         f.write("Created on {0} </br>".format(datetime.now()))
#     for i in articles:
#         with open(doc_path, "a") as f:
#             f.write(str(i))
